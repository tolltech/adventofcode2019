﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_202
    {
        [Test]
        [TestCase(@"
#######A#########  
#######.........#  
#######.#######.#  
#######.#######.#  
#######B#######.#  
#####       ###.#  
B..##       ###.#  
##.##       ###.#  
##..D       ###.#  
#####       ###.#  
#########F#####.#  
D.#######...###.#  
#.#########.###.#  
F.#########.....#  
###########Z#####", 26)]
        [TestCase(@"
#################A#############
#.#...#...................#.#.#
#.#.#.###.###.###.#########.#.#
#.#.#.......#...#.....#.#.#...#
#.#########.###.#####.#.#.###.#
#.............#.#.....#.......#
###.###########S###C#####.#.#.#
#.....#                 #.#.#.#
#######                 #####.#
#.#...#                 #.....T
#.#.#.#                 #.#####
#...#.#                 Y...#.#
#.###.#                 #####.#
I...#.#                 #.....#
#####.#                 #.###.#
Z.....#                 Q...#.S
###.###                 #######
O.#.#.#                 #.....#
#.#.#.#                 ###.#.#
#...#.I                 B...#.L
#####.#                 #.#####
Y.....#                 T.#...Q
#.###.#                 #.###.#
#.#...#                 #.....#
###.###                 #.#.###
#.....#                 #.#...#
#.###.#####O#L#####J#####.###.#
#...#.#.#...#.....#.....#.#...#
#.#####.###.###.#.#.#########.#
#...#.#.....#...#.#.#.#.....#.#
#.###.#####.###.###.#.#.#######
#.#.........#...#.............#
#########B###J###C#############", 0)]
        [TestCase(@"
###########Z#L#Q#W#######K###############  
#...#.......#.#.......#.#.......#.#.#...#  
###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
#.#...#.#.#...#.#.#...#...#...#.#.......#  
#.###.#######.###.###.#.###.###.#.#######  
#...#.......#.#...#...#.............#...#  
#.#########D#######E#I#######H#######.###  
#...#.#                           #.#.#.#  
#.###.#                           #.#.#.#  
#.#...#                           #...#.#  
#.###.#                           #.###.#  
#.#...O                           W.#.#.H
#.###.#                           #.#.#.#  
J.....#                           #.....#  
#######                           #######  
#.#...K                           #.....I
#.###.#                           #.###.#  
#.....#                           #...#.#  
###.###                           #.#.#.#  
X...#.#                           F.#.#.#  
#####.#                           #######  
#.....J                           M.#...#  
###.#.#                           #.###.#  
E...#.#                           #.....F
###.###                           #.#.#.#  
#.....#                           #.#.#.#  
###.###########X###Q#######L#########.###  
#.....#...#.....#.......#...#.....#.#...#  
#####.#.###.#######.#######.###.###.#.#.#  
#.......#.......#.#.#.#.#...#...#...#.#.#  
#####.###.#####.#.#.#.#.###.###.#.###.###  
#.......#.....#.#...#...............#...#  
#############A#O#D###M###################", 396)]   //AXKHWIFMLDQ WHKXOJEIFMLDQWHKXOJEIQDZ
                                                    //01234567891098765434567898765432210
        [TestCase(@"
###################################D#########C#######I#########H#######E#####W#####################################
#.......#.#...#.....#...#.....#.........#...#.....#.#.......#...#...........#.....#.................#.#...#.#.#.#.#
###.###.#.###.#####.#.#.#####.###.#####.#.#.#.#.###.#.#########.###.###.#######.###.#####.###.###.###.###.#.#.#.#.#
#...#.#...#.#...#.#...#.#.........#.....#.#.#.#...#.....#.#.....#.#...#.#.#.#.#.......#.#.#.#.#.#.#.#.#...#.#.....#
#.###.#.###.###.#.###.###.###.#####.#####.#.###.#####.#.#.#####.#.###.###.#.#.#.#.#####.###.###.###.#.###.#.#.#####
#.#...#.....................#.#.#.......#.#.........#.#.#.......#.....#.......#.#...#.#...#.#.#.#.......#.....#.#.#
#####.###.#.###.###.#.#.#.#.###.#######.#.#############.#.###.###.#.#.#####.#.#.#####.#.###.#.#.#.###.#.#####.#.#.#
#.........#.#...#...#.#.#.#.#.#.#.......#.#.......#.....#.#.#.#...#.#.#.#...#...#.#.....#...#.....#.#.#.#.....#...#
#.#.#######.#############.###.#.###.#.#.#.#.###.#.#.#.#.#.#.#####.#####.###.#####.#.#####.#####.###.#########.#.#.#
#.#.#...#...#.#.#...........#.......#.#.#.#.#.#.#.#.#.#.#.#...#.....#.....#.....#.....#...#.....#...#...........#.#
###.###.###.#.#.#######.#####.###.#######.#.#.#.#####.###.#.###.###.###.#######.#.#.#####.###.#.#.#.#######.#######
#.#...#.#...#.#...#.#.......#.#...#.#.#.....#.#.....#...#.....#.#...#.#.....#.#...#...........#.#.#.#...#.......#.#
#.#####.#####.#.#.#.###.#########.#.#.#.###.###.#######.#.#.#######.#.###.###.#.###.#.#.###.###.#.###.#####.#.###.#
#...#...........#...#.#.#.#.#.#.....#.#...#.#.....#.#.#.#.#.#.........#.........#...#.#...#...#.#...#.#.....#.#...#
#.#######.#####.###.#.###.#.#.#####.#.#.#######.#.#.#.#.###.###.#.###.#.#.#.#.#.###.#######.#####.###.#######.#.#.#
#.....#...#.#...#.............#.#.....#.#...#...#.#.....#.#...#.#.#.#.#.#.#.#.#...#...#.............#.#.#...#...#.#
#.#########.#####.#######.###.#.#.#########.###.###.###.#.#.#.###.#.#####.#######.###.###.#.#.#.#####.#.#.###.#####
#.#.#.#.#...#.#.#.#.......#.#.....#.....#.........#.#...#...#...#...#.#.......#.#.#.#.#...#.#.#.#.#.#.#...#.......#
#.#.#.#.###.#.#.#####.#.#.#.#####.#.#.#######.#.###.###.#.#.#.###.###.###.#####.###.#.###########.#.#.#.#.###.###.#
#.#...#.....#...#.#...#.#...#.#...#.#.....#...#...#.#...#.#.#.#.#...#.#.#.#.#...#...#.#.....#.#.......#.#...#.#...#
#.###.#.#######.#.#######.###.###.#.###.#######.#####.###.#.#.#.#.###.#.#.#.#.###.#######.###.#.#####.###.###.#####
#...........#.......#.#.#.#.........#.#...#.#.....#.....#.#.#.#...#.#.#...#...#...#...#.#...#.#.#...#...#...#.....#
###.###.###########.#.#.###.###.#.###.#.###.###.#######.#.#######.#.#.###.#.#.#.#.#.###.#.###.#####.#.###.###.#.###
#.....#...#.#.#.......#.#.#.#...#...#...#...#.#.#.......#.#.......#.#.#.....#.#.#.#...#.....#...#...#...#...#.#.#.#
###.#######.#.###.#####.#.#####.#.###.#####.#.#.#.#.#####.###.#.#.#.#.###.###.#.###.###.#.#.#.#####.###.#.###.###.#
#.#.....#.#.#...#...#...#.......#.#.#.....#.#...#.#...#.....#.#.#.......#...#.....#.....#.#...#.#...#.#.#.#.#.#.#.#
#.#.###.#.#.###.#.###.#########.###.###.###.#.#.#.#.#.###.#.###.#######.#######.###.#.#.#######.#.###.#.#.#.#.#.#.#
#.#...#.#...#.#...#.#...#.#...#...#.......#...#.#.#.#.#...#...#.#.......#...........#.#.#...#...#.#.#.....#.......#
#.#.#######.#.#.#.#.#.###.#.#######J###########N#P#######R#######H###########G#############.###.#.#.###.###.###.###
#...#...#.....#.#.#...#.#.#.#                                                         #.#.#.#...#...#.#...#.#.....#
###.#.#####.#.###.###.#.#.#.#                                                         #.#.#.#.###.###.#.#######.###
#.#.#.#.#.#.#...........#.#.#                                                         #.......#.........#.........#
#.#.#.#.#.#####.###.#####.#.#                                                         #.#####.#.###.###.#.#####.###
#...#...#...#.#.#.#.#.#.#...#                                                         #.....#.....#.#.#.....#.....Й
###.###.#.###.#.#.#.#.#.#.###                                                         #####.###.#####.#.###########
#.#.....#.....#.#.....#.....#                                                         #.......#.#.#.#.#...#.......#
#.###.#######.#####.#.#.#.###                                                         #.#####.###.#.#.#.#.#######.#
#.#...#.#...#.....#.#.#.#...#                                                         S.#...#.#.#.#.#.#.#.#.#.#.#.#
#.###.#.#.#####.###.#.#.#.###                                                         ###.#####.#.#.#.#####.#.#.#.#
B...#.#...#.........#...#...X                                                         #.....#...........#.......#.J
#.###.###.#####.###.#.#######                                                         #.#.#.#.#.#####.###.###.#.#.#
#.................#.#.#.#...#                                                         Ш.#.#...#.#...#.#.#.#.#.#...#
#.#.###.#####.###.#.###.#.###                                                         ###.#.#######.#.#.#.#.#####.#
#.#.#.#.#.#.#...#.#.#.#.....#                                                         #.#.#...#.........#.#...#...#
#.#.#.###.#.#########.#.###.#                                                         #.#.#######.#####.#.###.#####
#.#.#.#.#.#...#.......#.#...#                                                         #.#.#.....#.#.#.......#...#.#
#####.#.#.#.#.#######.#.#.###                                                         #.#####.#####.#########.###.#
#.#.#...#...#...#.#.....#...#                                                         #...#.....#...............#.R
#.#.#.###.#####.#.###.#.#.#.#                                                         #.#.#.#.###.#############.#.#
Ц.............#.......#.#.#.D                                                         C.#...#.#.....#.....#...#.#.#
#####.###.#.###.#############                                                         #.###.#.###.#####.#####.#.#.#
У...#.#...#.#...#...#.#.....Щ                                                         #...#.#.#.............#.....#
#.#####.#######.#.#.#.#.###.#                                                         #######.#.#.###.#############
#.....#.#.....#.#.#...#...#.W                                                         #.........#.#.#.#.#.#...#...#
#.#########.#####.###.#.#####                                                         #.###########.###.#.###.###.#
Я...#...#.#.........#.......#                                                         #.#...........#.....#.......N
#.#.#.#.#.#.#####.###.#######                                                         ###.#########.#.###.#.#.#####
#.#...#.......#.....#.....#.#                                                         #.#.........#...#.#...#.#.#.#
#######.###.###.###########.#                                                         #.#########.#####.#.#.###.#.#
#.....#...#.#.#.#.....#.#...#                                                         Y.........#.#...#...#...#...#
#.###.#######.###.#####.###.#                                                         ###.###.###.#.###.#########.#
#...#...#...#.#.....#.#.....Q                                                         #.#.#.........#...#...#...#.#
#.###.###.#.#.#.#.#.#.#.#.#.#                                                         #.#.#.#######.#####.###.###.#
#.#...#...#...#.#.#.#.#.#.#.#                                                         O.#.#.#...#.#.#.#.....#.#...#
#.#.###.#.###.###.#.#.#####.#                                                         #.#####.###.###.###.#.#.#.#.#
G.#.....#.#.......#.........#                                                         #...#...............#.....#.Y
#.###.#.#####.#.###########.#                                                         #.#####.#####.#######.###.#.#
#...#.#.#.#.#.#.#.#.......#.#                                                         #...#...#.#...#.#.......#.#.#
#.#######.#.#####.###.###.###                                                         #.#.#.###.###.#.#.###.#.#####
#.#.....#.....#...#...#.....#                                                         #.#.......#.....#.#...#.#...#
#####.###.#####.###.#.#.###.#                                                         ###.###.#####.#############.#
Ф.....#.#.#.#.#...#.#.#.#...#                                                         #.#...#.#.#.#...#...#.....#.Q
#.#.#.#.#.#.#.###.#.#######.#                                                         #.#######.#.###.###.###.#.#.#
#.#.#...............#.#...#.Я                                                         B...#...#.#...#.#.....#.#...#
#####################.#.#####                                                         #.#####.#.###.#####.###.###.#
#.....#...............#.....#                                                         #.#...#...#.......#...#...#.#
#.###.#.#########.###.#.###.#                                                         #.#.#####.###.#####.#.###.###
#.#.#.#...#.......#...#...#.Ф                                                         #...................#.....#.#
#.#.#.###.###.###.#.#####.###                                                         ###########################.#
#...#...#...#.#...#.#...#...#                                                         #.#.........................#
###.###.#.###.###.#.#.#.#.###                                                         #.#.#.###.#.#.###.###.###.#.#
З.....#...#.#.#...#...#.....#                                                         #.#.#.#.#.#.#.#.....#...#.#.#
###.#######.###.#####.###.#.#                                                         #.###.#.#.###.###.#########.#
#.......#.#.....#.#...#...#.#                                                         Г.#.#...#.#...#.....#.......#
###.###.#.###.###.###.#.#.###                                                         #.#.###.###.###.###.###.#.###
#.....#...#...#.......#.#.#.#                                                         #.........#.#...#.....#.#...Щ
###.#########.#####.#######.#                                                         #.#.###.#####.###.#########.#
#.........#.....#.....#.....#                                                         #.#.#...#.....#...#.#.......#
#.###.#.#####.#############.#####З#########E###I###########V#######У###Й#####Ц#########.#####.#####.###.#.#####.###
#...#.#...#.....#.......#.#.........#.#.#.#...#.....#.......#.........#.....#.........#...#.#...#...#.....#...#...#
#####.#.#.#######.#####.#.#.###.#.###.#.#.#.#####.#######.#.#.#.#####.###.#.#######.#######.#.#.#.###.###.###.#.###
#.....#.#...#...#...#.#.#...#.#.#.#.........#.....#.......#.#.#...#...#...#.#.#.....#.#...#...#.#...#.#...#.......#
###.#.#####.###.#.###.#.###.#.#.###.###.#######.#######.###.#.#######.###.#.#.#####.#.###.#.#.#######.###.###.###.#
#...#.....#...#...#.......#.#.....#.#...#.....#.#...#.....#.#.#.....#...#.#.....#.........#.#.......#.#.....#.#.#.#
#.#.#.#####.#######.#.#.#####.#.#######.#.#.###.#.#.#.#.#####.#.#.#######.#.###.###.###.###########.#.#.###.###.#.#
#.#.#...#...#.....#.#.#.......#...#.......#.#.#...#.#.#...#.#...#.....#...#...#.#.#.#.#.#...#...#...#.#.#.#.#.....#
###.#.#.#########.#######.#####.#.###.#####.#.#####.#.#####.#####.#######.#.#####.#.#.###.#.#.#####.#####.#######.#
#...#.#.#.................#...#.#.#...#.#.#...#...#.#.#.....#.#.......#...#.#.....#.......#.#.....#.#...#.........#
#.###.#####.###.###.#.#.#.###.#.#####.#.#.#.###.#.#.#.#.#.###.#.#.#.#######.#####.#.###.#.#####.#####.###.###.#.#.#
#.#.....#...#.....#.#.#.#.#.......#...#.....#...#...#...#.#.....#.#.#.....#.#...#.....#.#...............#...#.#.#.#
###.#.#######.#####.#####.###.#.#########.###.#.###.#.#####.#.#######.#.###.#.#####.###.#.#.#.#.###.#.###########.#
#.#.#...#.....#.......#...#.#.#.#.#.....#...#.#...#.#.#...#.#.#.#.#.#.#.........#.#...#.#.#.#.#...#.#.......#.....#
#.#.#.#.#####.#.#####.#.###.###.###.#.###.#.#.#######.#.#.###.#.#.#.###.#.#.#.###.#.###.###.###############.###.#.#
#.#.#.#.#.....#.#...#.#.#.....#.....#...#.#.#.#.#.....#.#.....#...#.....#.#.#...#.....#.#.#...........#.#.....#.#.#
#.###.#####.###.###.#.#.#.###.#.#.###.#.###.#.#.###.#.#####.#.#.###.#.###.###.###.#.#####.#####.#.#.###.#########.#
#.......#.#.#.#.#.....#.#.#.#.#.#.#...#.#...#.....#.#.#...#.#.#...#.#.#.....#...#.#.#.......#...#.#...........#...#
###.#.#.#.###.#.###.#######.#.#####.#######.###.#####.#.#####.#.#####.#.#.#.#.#.###.#.#.#.###.###.###.###.#####.#.#
#...#.#...#.....#...#.#.#.#.#...#...#.#.#...#...#.#.....#...#...#.#.#.#.#.#.#.#.#.....#.#...#.#...#...#...#.#...#.#
###.###.###.#.#.#####.#.#.#.###.#.###.#.#.#.#.#.#.###.###.###.#.#.#.#####.#########.###############.#.#####.###.#.#
#...#.#.#...#.#.#.#.#.#.#.#...........#.#.#.#.#...#.....#.....#.#.#.....#.#...#.....#.#.#...#.#.#.#.#...#.......#.#
###.#.#####.#####.#.#.#.#.#.#######.###.###.#.#######.###.#.#.###.#.#####.#.#######.#.#.#.###.#.#.#######.#.###.#.#
#.#.#...#.#.#.#.............#.#.#...#.......#.......#.#.#.#.#.#.#...#.......#.#.#.#...................#.#.#...#.#.#
#.###.###.###.#######.###.###.#.#.#########.#.#######.#.#####.#.#.#####.#####.#.#.#.###.###############.#####.#####
#.........#.........#.#.....#.....#.....#.#.#...#.#.#...#.....#...#.#...#...#.#...#.#.#.......#.#...#...#.....#...#
#####.###.###.#####.#####.#####.#######.#.#.###.#.#.###.#####.#.#.#.#.###.#.#.#.#.#.#.#.#######.#.###.#.#####.#.###
#.....#.......#.............#...........#...#.......#...#.......#.#.......#...#.#.....#...............#.#.........#
#################################A###X###Z#V#######S###P#######Ш###########Г#####O#################################", 526)]
        public void Test1(string input, int expected)
        {
            var lines = input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries).Select(x=>x.Trim()).ToArray();
            lines.Select(x => x.Length).Distinct().Count().Should().Be(1);

            var maxX = lines.First().Length;
            var maxY = lines.Length;

            YLength = maxY;
            XLength = maxX;

            var map = new Dictionary<(int X, int Y), string>();

            for (var y = 0; y < maxY; ++y)
            for (var x = 0; x < maxX; ++x)
            {
                map[(x, y)] = lines[y][x].ToString();
            }

            Solve(map).Should().Be(expected);
        }

        private int Solve(Dictionary<(int X, int Y), string> map)
        {
            var droid = map.Single(x => x.Value == "A").Key;
            var end = map.Single(x => x.Value == "Z").Key;

            var nodes = BuildNodes(map, droid, "A");

            //var result = StepsCount(("A", true), "Z", nodes, new Dictionary<(string Letter, bool isOnEdge, int Level), int>{{("A", true, 0), 0}}, 0);

            //if (result.Length == 0)
            //{
            //    return 0;
            //}

            //var orderedEnumerable = result.OrderBy(x => x.StepsCount).ToArray();
            //var stepsCount = orderedEnumerable.First().StepsCount;
            //return stepsCount;

            return StepsCountBFS(("A", true), "Z", nodes);
        }

        private static int? maxSteps = null;

        //private class Letter
        //{
        //    public string Letter { get; set; }
        //    public int Steps { get; set; }
        //    public int Level { get; set; }
        //    public bool IsOnEdge { get; set; }
        //    public Letter Parent { get; set; }
        //}

        private int StepsCountBFS((string Letter, bool IsOnEdge) from, string to,
            Dictionary<(string Letter, bool IsOnEdge), Node> nodes)
        {
            int? maxStepsBfs = null;
            var currentNodes = new Queue<(Node Node, int Steps, string Parent, int CurrentLevel, string[] Parents)>();
            currentNodes.Enqueue((nodes[@from], 0, null, 0, new string[0]));

            var visited = new HashSet<(string Letter, bool IsOnEdge, int Level)>();
            while (currentNodes.Count > 0)
            {
                var currentNode = currentNodes.Dequeue();

                var hash = (currentNode.Node.Value, currentNode.Node.IsOnEdge, currentNode.CurrentLevel);
                if (visited.Contains(hash) || currentNode.CurrentLevel > 100)
                {
                    continue;
                }

                if (currentNode.Node.Value == to && currentNode.CurrentLevel == 0)
                {
                    if (!maxStepsBfs.HasValue || currentNode.Steps < maxStepsBfs.Value)
                    {
                        maxStepsBfs = currentNode.Steps;
                    }

                    return currentNode.Steps;
                }

                visited.Add(hash);

                var childs = currentNode.Node.Neighbours
                    .Where(child => child.Value != currentNode.Parent && child.Value != "A")
                    .Where(child => currentNode.CurrentLevel + child.DeltaLevel >= 0)
                    .Where(child => !visited.Contains((child.Value, IsOnEdge(child.Position),
                        currentNode.CurrentLevel + child.DeltaLevel)))
                    .Select(child => (nodes[(child.Value, IsOnEdge(child.Position))],
                        currentNode.Steps + child.StepsCount,
                        currentNode.Node.Value, currentNode.CurrentLevel + child.DeltaLevel,
                        currentNode.Parents.Concat(new[] {currentNode.Node.Value}).ToArray()))
                    .OrderBy(x => x.Item2)
                    .ToArray();

                foreach (var child in childs)
                {
                    currentNodes.Enqueue(child);
                }
            }

            return maxStepsBfs ?? 0;
        }

        private (string[] Path, int StepsCount)[] StepsCount((string Letter, bool IsOnEdge) from, string to, Dictionary<(string Letter, bool IsOnEdge), Node> nodes, Dictionary<(string Letter, bool isOnEdge, int Level), int> visited, int level, int currentSteps = 0)
        {
            if (maxSteps.HasValue && currentSteps >= maxSteps)
            {
                return Array.Empty<(string[] Path, int StepsCount)>();
            }

            if (from.Letter == to)
            {
                if (!maxSteps.HasValue || maxSteps.Value > currentSteps)
                {
                    maxSteps = currentSteps;
                }

                return new[] {(visited.Keys.Select(x => x.Letter).ToArray(), 0)};
            }

            var result = new List<(string[], int)>();

            var neighbours = nodes[from].Neighbours
                .Where(x => level != 0 || !IsOnEdge(x.Position) || x.Value == to || x.Value == from.Letter)
                .OrderBy(x => x.Value == to ? 0 : 1).ThenBy(x => level * x.DeltaLevel < 0 ? 0 : 1)
                .ToArray();
            foreach (var neighbour in neighbours)
            {
                if (neighbour.Value == "A")
                {
                    continue;
                }

                var newLevel = level + neighbour.DeltaLevel;
                if (IsCycled(visited, neighbour.Value, IsOnEdge(neighbour.Position), newLevel))
                {
                    continue;
                }

                if (neighbour.Value == to && level != 0)
                {
                    continue;
                    //var previousLetter = visited.LastOrDefault(x => x.Key.Letter == neighbour.Value && x.Key.isOnEdge == IsOnEdge(neighbour.Position) && x.Key.Level != level);
                    //if (previousLetter.Key.Letter == null)
                    //    continue;

                    //var delta = level - previousLetter.Key.Level;
                    //if (delta > 0)
                    //    continue;

                    //if (level % delta != 0)
                    //    continue;

                    //var repeatCount = level / delta;
                    //var chainForRepeat = visited.SkipWhile(x => x.Key != previousLetter.Key).ToArray();

                    //var list = new List<((string Letter, bool isOnEdge, int Level) Key, int Value)>();
                    //for (var i = 0; i < repeatCount; ++i)
                    //{
                    //    list.AddRange(chainForRepeat.Select(x => (x.Key, x.Value)));
                    //}

                    //var steps = currentSteps + list.Sum(x => x.Value);
                    //if (!maxSteps.HasValue || maxSteps.Value > steps)
                    //{
                    //    maxSteps = steps;
                    //}

                    //return new[] {(list.Select(x => x.Key.Letter).ToArray(), steps)};
                }


                visited[(neighbour.Value, IsOnEdge(neighbour.Position), newLevel)] = neighbour.StepsCount;

                var childResult = StepsCount((neighbour.Value, IsOnEdge(neighbour.Position)), to, nodes, visited, newLevel, currentSteps + neighbour.StepsCount);
                result.AddRange(childResult.Select(x => (x.Path, neighbour.StepsCount + x.StepsCount)));

                visited.Remove((neighbour.Value, IsOnEdge(neighbour.Position), newLevel));
            }

            return result.ToArray();
        }

        private bool IsCycled(Dictionary<(string Letter, bool isOnEdge, int Level), int> visited, string neighbourValue, bool isOnEdge, int level)
        {
            //todo: дописать циклы
            if (visited.ContainsKey((neighbourValue, isOnEdge, level)))
            {
                return true;
            }

            var lastVisited = visited.Last();
            if (lastVisited.Key.Letter != neighbourValue)
            {
                return false;
            }

            var visitedArray = visited.Keys.ToArray();
            for (var i = visitedArray.Length - 1; i > 0; --i)
            {
                var current = visitedArray[i];
                var prevCurrent = visitedArray[i - 1];

                if (current.Letter != prevCurrent.Letter || current.Letter != neighbourValue || current.isOnEdge != isOnEdge)
                {
                    continue;
                }

                if (current.Level < level)
                {
                    return true;
                }
            }

            return false;
        }

        private Dictionary<(string Letter, bool IsOnEdge), Node> BuildNodes(Dictionary<(int X, int Y), string> map, (int X, int Y) droid, string letter)
        {
            var nodes = new Dictionary<(string Letter, bool IsOnEdge), Node>();

            var positions = new Stack<(string Letter, (int X, int Y) Position)>();
            positions.Push((letter, droid));

            while (positions.Count > 0)
            {
                var position = positions.Pop();

                if (nodes.ContainsKey((position.Letter, IsOnEdge(position.Position))))
                {
                    continue;
                }

                var currentNode = new Node
                {
                    Value = position.Letter,
                    IsOnEdge = IsOnEdge(position.Position)
                };
                nodes[(currentNode.Value, currentNode.IsOnEdge)] = currentNode;

                var oppositeNode = map.SingleOrDefault(x => x.Value == currentNode.Value && IsOnEdge(x.Key) != currentNode.IsOnEdge);
                if (oppositeNode.Value != null)
                    currentNode.Neighbours.Add((Value: oppositeNode.Value, StepsCount: 1, oppositeNode.Key,
                        IsOnEdge(oppositeNode.Key) ? 1 : -1));

                var nextDoorOrKeys = GetNextDoorOrKeys(position.Position, 0, map, new HashSet<(int X, int Y)>());
                foreach (var nextDoorOrKey in nextDoorOrKeys)
                {
                    currentNode.Neighbours.Add(nextDoorOrKey);

                    if (nextDoorOrKey.Value == "Z" || nextDoorOrKey.Value == "A")
                    {
                        continue;
                    }

                    var oppositePortal = map.Single(x => x.Value == nextDoorOrKey.Value && x.Key != nextDoorOrKey.Position);
                    positions.Push((nextDoorOrKey.Value, nextDoorOrKey.Position));
                    positions.Push((nextDoorOrKey.Value, oppositePortal.Key));
                }
            }

            nodes[("Z", true)] = new Node
            {
                Value = "Z",
                IsOnEdge = true
            };

            return nodes;
        }

        private (string Value, int StepsCount, (int X, int Y) Position, int DeltaLevel)[] GetNextDoorOrKeys((int X, int Y) currentLevelCell,
            int currentLevel, Dictionary<(int X, int Y), string> map, HashSet<(int X, int Y)> visited)
        {
            var neighbours = directions
                .Select(direction => Step(currentLevelCell, direction))
                .Where(x => !visited.Contains(x))
                .Where(x => map.TryGetValue(x, out var val) && val != "#")
                .Select(x => (Position: x, Value: map[x]))
                .ToArray();

            if (neighbours.Length == 0)
            {
                return Array.Empty<(string Value, int StepsCount, (int X, int Y) Position, int DeltaLevel)>();
            }

            var list = new List<(string Value, int StepsCount, (int X, int Y) Position, int DeltaLevel)>();
            list.AddRange(neighbours.Where(x => char.IsLetter(x.Value.Single())).Select(x => (x.Value, currentLevel + 1, x.Position, 0)));

            var points = neighbours.Where(x => x.Value == ".").Select(x => x.Position).ToArray();

            visited.Add(currentLevelCell);

            list.AddRange(points.SelectMany(x => GetNextDoorOrKeys(x, currentLevel + 1, map, visited)));

            visited.Remove(currentLevelCell);

            return list.ToArray();
        }

        private bool IsOnEdge((int X, int Y) position)
        {
            return position.X == 0 || position.Y == 0 || position.X == XLength - 1 || position.Y == YLength - 1;
        }

        private int XLength = 0;
        private int YLength = 0;

        private string RenderMap(Dictionary<(int X, int Y), string> map, Dictionary<(int X, int Y), int> markedMap)
        {
            var maxX = map.Keys.Select(x => x.X).Max();
            var maxY = map.Keys.Select(x => x.Y).Max();
            var sb = new StringBuilder();
            for (var y = 0; y <= maxY; ++y)
            {
                for (var x = 0; x <= maxX; ++x)
                {
                    sb.Append(map[(x, y)]);
                    //if (markedMap.TryGetValue((x, y), out var marked))
                    //{
                    //    sb.Append(marked);
                    //}
                    //else
                    //{
                    //    sb.Append(map[(x, y)]);
                    //}
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        private (int X, int Y) Step((int X, int Y) droid, (int X, int Y) direction)
        {
            return (X: droid.X + direction.X, Y: droid.Y + direction.Y);
        }

        private readonly (int X, int Y)[] directions = new[]
        {
            Up,
            Right,
            Down,
            Left,
        };

        private static readonly (int X, int Y) Up = (0, -1);
        private static readonly (int X, int Y) Left = (-1, 0);
        private static readonly (int X, int Y) Down = (0, 1);
        private static readonly (int X, int Y) Right = (1, 0);

        private class Node
        {
            public Node()
            {
                Neighbours = new List<(string Value, int StepsCount, (int X, int Y) Positio, int DeltaLevel)>();
            }

            public string Value { get; set; }
            public bool IsOnEdge { get; set; }
            public List<(string Value, int StepsCount, (int X, int Y) Position, int DeltaLevel)> Neighbours { get; set; }

            public bool IsKey => char.IsLower(Value.Single());
            public bool IsDoor => char.IsUpper(Value.Single());
        }
    }
}