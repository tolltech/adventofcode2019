﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task6
    {
        [Test]
        [TestCase(@"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L", 42)]
        [TestCase(@"COM)a1
COM)b1
a1)c2", 4)]
        [TestCase(@"COM)1
1)2
2)3
3)4", 10)]
        [TestCase(@"COM)1", 1)]
        [TestCase(@"COM)1
COM)2
COM)3
COM)4
2)5
2)6
2)7", 10)]
        public void Test1(string input, int expected)
        {
            Solve(input).Should().Be(expected);
        }

        private static string ParseInput(string filename)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadAllText(path);
        }

        [Test]
        public void Test61()
        {
            var actual = Solve(ParseInput("61.txt"));
            actual.Should().Be(151345);
        }

        private static int Solve(string input)
        {
            var lines = input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            var nodes = lines.Select(x =>
            {
                var splits = x.Split(new[] {")"}, StringSplitOptions.RemoveEmptyEntries);
                return (Parent: splits[0], Child: splits[1]);
            }).ToArray();

            nodes.Any(x => x.Child == "COM").Should().BeFalse();
            nodes.Any(x => x.Parent == "COM").Should().BeTrue();
            nodes.Distinct().Count().Should().Be(nodes.Length);

            var lookUp = nodes.ToLookup(x => x.Parent, x => x.Child);
            var tree = BuildTree("COM", lookUp);
            return tree.Rool().Sum(x => x.LevelNumber);
        }

        private static Node<string> BuildTree(string node, ILookup<string, string> lookUp, int currentLevel = 0)
        {
            return new Node<string>
            {
                LevelNumber = currentLevel,
                Value = node,
                Childs = lookUp[node].Select(x => BuildTree(x, lookUp, currentLevel + 1)).ToArray()
            };
        }

        private class Node<T>
        {
            public Node()
            {
                Childs = Array.Empty<Node<T>>();
            }

            public Node<T>[] Childs { get; set; }
            public int LevelNumber { get; set; }
            public T Value { get; set; }

            public IEnumerable<Node<T>> Rool()
            {
                yield return this;
                foreach (var child in Childs.SelectMany(x => x.Rool()))
                {
                    yield return child;
                }
            }
        }
    }
}