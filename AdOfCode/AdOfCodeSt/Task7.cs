﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task7
    {
        [Test]
        [TestCase(new[] {3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0}, 43210, new[] {4, 3, 2, 1, 0})]
        [TestCase(new[]
        {
            3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23,
            101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0
        }, 54321, new[] {0, 1, 2, 3, 4})]
        [TestCase(new[]
        {
            3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33,
            1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0
        }, 65210, new[] {1, 0, 4, 3, 2})]
        public void Test1(int[] inputs, int maxSignal, int[] bestPhase)
        {
            var ampInput = GetOutput(inputs, bestPhase);
            ampInput.Should().Be(maxSignal);
        }

        [Test]
        [TestCase(new[] {3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0}, 43210, new[] {4, 3, 2, 1, 0})]
        [TestCase(new[]
        {
            3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23,
            101, 5, 23, 23, 1, 24, 23, 23, 4, 23, 99, 0, 0
        }, 54321, new[] {0, 1, 2, 3, 4})]
        [TestCase(new[]
        {
            3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33,
            1002, 33, 7, 33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0
        }, 65210, new[] {1, 0, 4, 3, 2})]
        [TestCase(new[]
        {
            3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 72, 85, 110, 191, 272, 353, 434, 99999, 3, 9, 102, 4, 9, 9,
            101, 2, 9, 9, 102, 3, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 4, 9, 102, 2, 9, 9, 1001, 9, 5, 9, 1002, 9, 5, 9, 101,
            3, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 99, 3, 9, 1001, 9, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 101, 2,
            9, 9, 102, 2, 9, 9, 1001, 9, 2, 9, 1002, 9, 4, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101,
            2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1002, 9,
            2, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102,
            2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001,
            9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9,
            1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9,
            101, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3,
            9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9,
            1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3,
            9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            101, 2, 9, 9, 4, 9, 99
        }, 17790, new[] {2,1,3,4,0})]
        public void TestBest(int[] inputs, int maxSignal, int[] bestPhases)
        {
            var best = (Output: int.MinValue, Phases: new[] {0, 0, 0, 0, 0});
            for (var i1 = 0; i1 < 5; i1++)
            for (var i2 = 0; i2 < 5; i2++)
            for (var i3 = 0; i3 < 5; i3++)
            for (var i4 = 0; i4 < 5; i4++)
            for (var i5 = 0; i5 < 5; i5++)
            {
                var phases = new[] {i1, i2, i3, i4, i5};

                if (phases.Distinct().Count() != 5)
                {
                    continue;
                }

                var ampInput = GetOutput(inputs, phases);

                if (best.Output <= ampInput)
                {
                    best = (ampInput, phases);
                }
            }

            best.Output.Should().Be(maxSignal);
            string.Join(string.Empty, best.Phases).Should().Be(string.Join(string.Empty, bestPhases));
        }

        private static int GetOutput(int[] programm, int[] phases)
        {
            var ampInput = 0;
            foreach (var phase in phases)
            {
                var listOuputs = new List<int>();
                var programmCopy = programm.ToArray();

                var input = new Stack<int>();
                input.Push(ampInput);
                input.Push(phase);

                Solve(programmCopy, input, listOuputs);
                var last = listOuputs.Last();

                listOuputs.RemoveAt(listOuputs.Count - 1);

                listOuputs.Should().AllBeEquivalentTo(0);

                ampInput = last;
            }

            return ampInput;
        }

        private static void Solve(int[] programInstructions, Stack<int> inputs, List<int> outputs)
        {
            for (var i = 0; i < programInstructions.Length;)
            {
                var instruction = programInstructions[i];
                var opCode = instruction % 100;

                if (opCode == 99)
                {
                    return;
                }

                var p1Mode = (Mode) ((instruction / 100) % 10);
                var p2Mode = (Mode) ((instruction / 1000) % 100);
                var p3Mode = (Mode) ((instruction / 10000) % 1000);

                if (p3Mode != Mode.Position)
                {
                    throw new Exception("Mode!");
                }

                var p1Instruction = programInstructions[i + 1];
                var p1 = p1Mode == Mode.Immediate ? p1Instruction : programInstructions[p1Instruction];

                if (opCode == 3 || opCode == 4)
                {
                    switch (opCode)
                    {
                        case 3:
                            programInstructions[p1Instruction] = inputs.Pop();
                            break;
                        case 4:
                            outputs.Add(p1);
                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 2;
                    continue;
                }

                if (opCode == 5 || opCode == 6)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    switch (opCode)
                    {
                        case 5:
                            if (p1 != 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        case 6:
                            if (p1 == 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 3;
                    continue;
                }

                if (opCode == 1 || opCode == 2 || opCode == 7 || opCode == 8)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    var p3Instruction = programInstructions[i + 3];

                    switch (opCode)
                    {
                        case 1:
                            programInstructions[p3Instruction] = p1 + p2;
                            break;
                        case 2:
                            programInstructions[p3Instruction] = p1 * p2;
                            break;
                        case 7:
                            programInstructions[p3Instruction] = p1 < p2 ? 1 : 0;
                            break;
                        case 8:
                            programInstructions[p3Instruction] = p1 == p2 ? 1 : 0;
                            break;
                        default:
                            throw new Exception("OpcOde");
                    }

                    i += 4;
                    continue;
                }

                throw new Exception();
            }

            throw new Exception("baaad");
        }

        private enum Mode
        {
            Position = 0,
            Immediate = 1
        }
    }
}