﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task12
    {
        [Test]
        [TestCase(12, 2)]
        [TestCase(14, 2)]
        [TestCase(1969, 966)]
        [TestCase(100756, 50346)]
        public void Test1(int input, int expected)
        {
            Solve(input).Should().Be(expected);
        }

        private static IEnumerable<T> ParseInput<T>(string filename, Func<string, T> parseLine)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadLines(path).Select(parseLine);
        }

        [Test]
        public void Test11()
        {
            var actual = Solve(ParseInput("11.txt", int.Parse).ToArray());
            actual.Should().Be(5062623);
        }

        private static int Solve(params int[] inputs)
        {
            if (inputs.Length == 0)
            {
                return 0;
            }

            var results = inputs.Select(x => x / 3 - 2).Where(x => x > 0).ToArray();
            return results.Sum() + Solve(results);
        }
    }
}