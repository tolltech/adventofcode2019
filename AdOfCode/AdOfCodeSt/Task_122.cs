﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_122
    {
        [Test]
        [TestCase(new[]
        {
            "-6,2,-9",
            "12,-14,-4",
            "9,5,-6",
            "-1,-4,9",
        }, 467081194429464L)]
        [TestCase(new[]
        {
            "-1,0,2",
            "2,-10,-7",
            "4,-8,8",
            "3,5,-1",
        }, 2772)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 4686774924L)]
        public void Test1(string[] input, long expectedSteps)
        {
            var moons = input.Select(x => x.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                .Select(x => new XYZ(x.Select(long.Parse).ToArray()))
                .Select(x => new Moon
                {
                    Coordinates = x,
                    CurrentVelocities = new XYZ(new long[3]),
                    DeltaVelocities = new XYZ(new long[3])
                })
                .ToArray();

            Solve(moons, expectedSteps).Should().Be(expectedSteps);
        }

        private long Solve(Moon[] moons, long steps)
        {
            var pairs = new (Moon Left, Moon Right)[]
            {
                (moons[0], moons[1]),
                (moons[0], moons[2]),
                (moons[0], moons[3]),
                (moons[1], moons[2]),
                (moons[1], moons[3]),
                (moons[2], moons[3]),
            };

            var periods = new Dictionary<int,long>();

            var firstCoordinates = moons.Select(x => new XYZ(x.Coordinates.Axes)).ToArray();
            for (var axeIndex = 0; axeIndex < 3; ++axeIndex)
            {
                for (var step = 0; ; ++step)
                {
                    SetDeltaVelocities(pairs, axeIndex);
                    foreach (var moon in moons)
                    {
                        MoveStep(moon, axeIndex);
                    }

                    var isStartPosition = true;
                    for (var moonIndex = 0; moonIndex < moons.Length; ++moonIndex)
                    {
                        var firstMoon = firstCoordinates[moonIndex].Axes;
                        if (firstMoon[axeIndex] != moons[moonIndex].Coordinates.Axes[axeIndex] ||
                            moons[moonIndex].CurrentVelocities.Axes[axeIndex] != 0)
                        {
                            isStartPosition = false;
                        }
                    }

                    if (isStartPosition)
                    {
                        periods[axeIndex] = step + 1;
                        break;
                    }
                }
            }

            var periodsLong = periods.Select(y => y.Value).Distinct().ToArray();
            return NOK(periodsLong);
        }

        //long NOD(long a, long b){
        //    while (a != b){
        //        if (a > b){
        //            a -= b;
        //        } else {
        //            b -= a;
        //        }
        //    }
        //    return a;
        //}

        //long NOK(long a, long b){
        //    return Math.Abs(a * b)/NOD(a, b);
        //}

        static long NOD(long a, long b)
        {
            if (b < 0)
                b = -b;
            if (a < 0)
                a = -a;
            while (b > 0)
            {
                var temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }
        static long NOK(long a, long b)
        {
            return Math.Abs(a*b)/NOD(a, b);
        }    

        long NOK(long[] longs)
        {
            var nok = NOK(longs[0], longs[1]);
            for (var i = 2; i < longs.Length; ++i)
            {
                nok = NOK(nok, longs[i]);
            }

            return nok;
        }

        private void MoveStep(Moon moon, int axeIndex)
        {
            moon.CurrentVelocities.Axes[axeIndex] += moon.DeltaVelocities.Axes[axeIndex];
            moon.Coordinates.Axes[axeIndex] += moon.CurrentVelocities.Axes[axeIndex];
        }

        private void SetDeltaVelocities((Moon Left, Moon Right)[] pairs, int axeIndex)
        {
            foreach (var pair in pairs)
            {
                pair.Left.DeltaVelocities.Axes[axeIndex] = 0;
                pair.Right.DeltaVelocities.Axes[axeIndex] = 0;
            }

            foreach (var pair in pairs)
            {
                var moon = pair.Left;
                var other = pair.Right;

                if (moon.Coordinates.Axes[axeIndex] > other.Coordinates.Axes[axeIndex])
                {
                    moon.DeltaVelocities.Axes[axeIndex]--;
                    other.DeltaVelocities.Axes[axeIndex]++;
                }

                if (moon.Coordinates.Axes[axeIndex] < other.Coordinates.Axes[axeIndex])
                {
                    moon.DeltaVelocities.Axes[axeIndex]++;
                    other.DeltaVelocities.Axes[axeIndex]--;
                }
            }
        }


        private class XYZ
        {
            public XYZ(long[] axes)
            {
                if (axes.Length != 3)
                {
                    throw new Exception("Baad");
                }

                Axes = axes.ToArray();
            }

            public readonly long[] Axes;

            public override string ToString()
            {
                return $"{Axes[0]},{Axes[1]},{Axes[2]}";
            }

            public bool MyEquals(XYZ other)
            {
                return this.Axes[0] == other.Axes[0]
                       && this.Axes[1] == other.Axes[1]
                       && this.Axes[2] == other.Axes[2];
            }
        }


        private class Moon
        {
            public XYZ Coordinates { get; set; }
            public XYZ CurrentVelocities { get; set; }
            public XYZ DeltaVelocities { get; set; }

            public bool MyEquals(Moon other)
            {
                return this.Coordinates.MyEquals(other.Coordinates)
                       && this.CurrentVelocities.MyEquals(other.CurrentVelocities);
            }
        }
    }
}