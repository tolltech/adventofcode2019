﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_22_2
    {
        [Test]
        [TestCase(10, @"deal with increment 3", 7, 1)]
        [TestCase(10, @"deal into new stack", 7, 2)]
        [TestCase(10, @"cut 3", 7, 4)]
        [TestCase(10, @"cut -4", 7, 1)]
        [TestCase(10, @"deal with increment 7
deal into new stack
deal into new stack", 7, 9)]
        [TestCase(10, @"cut 6
deal with increment 7
deal into new stack", 7, 2)]
        [TestCase(10, @"deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1", 7, 6)]
        [TestCase(10007, @"cut 1470
deal with increment 46
cut -6481
deal with increment 70
cut 547
deal with increment 48
cut -6479
deal with increment 69
cut -5203
deal with increment 13
deal into new stack
deal with increment 73
deal into new stack
cut -6689
deal with increment 61
cut -9853
deal with increment 48
cut -9673
deal into new stack
deal with increment 3
deal into new stack
deal with increment 64
cut 5789
deal with increment 66
deal into new stack
deal with increment 70
cut -2588
deal with increment 6
deal into new stack
deal with increment 6
cut -7829
deal with increment 49
deal into new stack
deal with increment 19
cut 9777
deal into new stack
deal with increment 27
cut 6210
deal into new stack
deal with increment 12
cut 6309
deal with increment 12
cut -9458
deal with increment 5
cut 6369
deal with increment 27
cut 2278
deal with increment 42
cut 6656
deal with increment 62
cut -1424
deal with increment 25
deal into new stack
deal with increment 12
deal into new stack
cut -7399
deal into new stack
cut -8925
deal with increment 47
deal into new stack
cut 5249
deal with increment 65
cut -213
deal into new stack
cut 6426
deal with increment 22
cut -6683
deal with increment 38
deal into new stack
deal with increment 62
cut 6855
deal with increment 75
cut 4965
deal into new stack
cut -5792
deal with increment 30
cut 9250
deal with increment 19
cut -948
deal with increment 26
cut -5123
deal with increment 68
cut -604
deal with increment 41
deal into new stack
deal with increment 45
cut 5572
deal into new stack
cut 3853
deal with increment 21
cut 1036
deal into new stack
deal with increment 6
cut 8114
deal into new stack
deal with increment 38
cut -5
deal with increment 58
cut 9539
deal with increment 19", 2019, 1822)]
        public void Test1(int cardsCount, string commandsStr, int expectedCard = 0, int expectedNumber = 0)
        {
            //for (var i = 0L; i < 101741582076661L; ++i)
            //{
            //    var s = i;
            //}

            var commands = commandsStr.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x =>
                {
                    if (x.Contains("deal with increment "))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.Increment,
                            Parameter = int.Parse(x.Replace("deal with increment ", string.Empty))
                        };
                    }

                    if (x.Contains("cut "))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.Cut,
                            Parameter = int.Parse(x.Replace("cut ", string.Empty))
                        };
                    }

                    if (x.Contains("deal into new stack"))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.NewStack,
                            Parameter = 0
                        };
                    }

                    throw new Exception("Baad!");
                })
                .ToArray();


            var card = expectedCard;
            foreach (var command in commands)
            {
                card = command.Shuffle(cardsCount, card);
            }

            if (expectedCard != 0)
            {
                card.Should().Be(expectedNumber);
            }
        }

        private class ShuffleCommand
        {
            public ShuffleType ShuffleType { get; set; }
            public int Parameter { get; set; }

            public int Shuffle(int stackLength, int position)
            {
                switch (ShuffleType)
                {
                    case ShuffleType.NewStack:
                        return stackLength - position - 1;
                    case ShuffleType.Cut:
                        var p = Parameter > 0 ? Parameter : stackLength + Parameter;
                        if (position >= p)
                        {
                            return position - p;
                        }
                        else
                        {
                            return stackLength - p + position;
                        }
                    case ShuffleType.Increment:
                        return (position * Parameter) % stackLength;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private enum ShuffleType
        {
            NewStack = 1,
            Cut = 2,
            Increment = 3
        }

        private static int Solve(params int[] inputs)
        {
            return inputs.Sum(x => x / 3 - 2);
        }
    }
}