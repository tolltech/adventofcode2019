﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_162
    {
        [Test]
        [TestCase(
            @"59791911701697178620772166487621926539855976237879300869872931303532122404711706813176657053802481833015214226705058704017099411284046473395211022546662450403964137283487707691563442026697656820695854453826690487611172860358286255850668069507687936410599520475680695180527327076479119764897119494161366645257480353063266653306023935874821274026377407051958316291995144593624792755553923648392169597897222058613725620920233283869036501950753970029182181770358827133737490530431859833065926816798051237510954742209939957376506364926219879150524606056996572743773912030397695613203835011524677640044237824961662635530619875905369208905866913334027160178",
            37615297)]
        //[TestCase(
        //    @"03036732577212944063491565474664",
        //    84462026)]
        //[TestCase(
        //    @"02935109699940807407585447034323",
        //    78725270)]
        //[TestCase(
        //    @"03081770884921959731165446850517",
        //    53553731)]
        //[TestCase(
        //    @"12345678",
        //    0)]
        //[TestCase(
        //    @"12345678",
        //    48226158, 1, 0, 1, TestName = "Regress1")]
        //[TestCase(
        //    @"12345678",
        //    34040438, 1, 0, 2, TestName = "Regress2")]
        //[TestCase(
        //    @"12345678",
        //    03415518, 1, 0, 3, TestName = "Regress3")]
        //[TestCase(
        //    @"12345678",
        //    01029498, 1, 0, 4, TestName = "Regress4")]
        //[TestCase(
        //    @"12345678",
        //    02949801, 1, 2, 4, TestName = "Regress5")]
        //[TestCase(
        //    @"59791911701697178620772166487621926539855976237879300869872931303532122404711706813176657053802481833015214226705058704017099411284046473395211022546662450403964137283487707691563442026697656820695854453826690487611172860358286255850668069507687936410599520475680695180527327076479119764897119494161366645257480353063266653306023935874821274026377407051958316291995144593624792755553923648392169597897222058613725620920233283869036501950753970029182181770358827133737490530431859833065926816798051237510954742209939957376506364926219879150524606056996572743773912030397695613203835011524677640044237824961662635530619875905369208905866913334027160178",
        //    45834272, 1, 0, TestName = "Regress6")]
        //[TestCase(
        //    @"80871224585914546619083218645595",
        //    24176176, 1, 0, TestName = "Regress7")]
        //[TestCase(
        //    @"19617804207202209144916044189917",
        //    73745418, 1, 0, TestName = "Regress8")]
        //[TestCase(
        //    @"69317163492948606335995924319873",
        //    52432133, 1, 0, TestName = "Regress9")]
        [TestCase(
            @"1234",
            62841234, 4, 8, TestName = "Regress10")]
        public void Test1(string input, int expected, int repeatCount = 10000, int? offset = null, int phases = 100)
        {
            var inputs = input.Select(x => int.Parse(x.ToString())).ToArray();
            //my offset = 5979191
            offset = offset ?? int.Parse(string.Join(string.Empty, inputs.Take(7)));
            Solve(inputs, phases, offset.Value, repeatCount).Should().Be(expected);
        }

        private int Solve(int[] inputs, int phaseNumber, int offset, int repeatCount)
        {
            var totalLength = inputs.Length * repeatCount;

            var currentPhase = new int[totalLength];
            for (var i = 0; i < totalLength; ++i)
            {
                currentPhase[i] = inputs[i % inputs.Length];
            }

            for (var phase = 0; phase < phaseNumber; ++phase)
            {
                var ints = new int[totalLength];
                var acc = 0;
                for (var i = totalLength; i > totalLength - offset; --i)
                {
                    acc += currentPhase[i - 1];
                    acc %= 10;
                    ints[i - 1] = acc;
                }

                currentPhase = ints;
            }

            return int.Parse(string.Join(string.Empty, currentPhase.Skip(offset).Take(8)));
        }
    }
}