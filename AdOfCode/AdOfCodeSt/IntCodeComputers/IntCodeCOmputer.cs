﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdOfCodeSt.IntCodeComputers
{
    public class IntCodeComputer
    {
        private Dictionary<long, long> programInstructions;

        public void LoadProgramm(Dictionary<long, long> programm)
        {
            this.programInstructions = programm.ToDictionary(x => x.Key, x => x.Value);
        }

        public void LoadProgramm(long[] programm)
        {
            this.programInstructions = programm.Select((x, i) => (x, i)).ToDictionary(x => (long) x.i, x => x.x);
        }

        public void LoadProgramm(int[] programm)
        {
            LoadProgramm(programm.Select(x => (long) x).ToArray());
        }

        public long[] Compute(Stack<long> input)
        {
            var outputs = new List<long>();
            Solve(input, outputs);
            return outputs.ToArray();
        }

        public long[] Compute()
        {
            return Compute(new Stack<long>());
        }

        public long[] Compute(out string output, params string[] ascii)
        {
            var bytes = Encoding.ASCII.GetBytes(string.Join("\n", ascii) + "\n").Select(x => (long) x).ToArray();
            var result = Compute(bytes);
            output = Encoding.ASCII.GetString(result.Select(x => (byte) x).ToArray());
            return result;
        }

        public long[] Compute(long? input)
        {
            if (input.HasValue)
            {
                return Compute(new[] {input.Value});
            }

            return Compute(Array.Empty<long>());
        }

        public long[] Compute(long[] input)
        {
            var stack = new Stack<long>();

            if (input != null)
            {
                foreach (var l in input.Reverse())
                {
                    stack.Push(l);
                }
            }

            return Compute(stack);
        }

        private long lastInstructionIndex = 0L;
        private long relativeBase = 0L;
        public bool End { get; private set; }

        private void Solve(Stack<long> input, List<long> outputs)
        {
            for (long i = lastInstructionIndex;;)
            {
                lastInstructionIndex = i;
                var instruction = programInstructions[i];
                var opCode = (OpCode) (instruction % 100);
                if (opCode == OpCode.End)
                {
                    End = true;
                    return;
                }

                var p1Mode = (Mode) ((instruction / 100) % 10);
                var p2Mode = (Mode) ((instruction / 1000) % 10);
                var p3Mode = (Mode) ((instruction / 10000) % 10);

                if (p3Mode == Mode.Immediate)
                {
                    throw new Exception("Mode!");
                }

                var p1Instruction = programInstructions[i + 1];

                var p1 = GetParamValue(p1Mode, p1Instruction, programInstructions, relativeBase);

                if (opCode == OpCode.Read || opCode == OpCode.Write || opCode == OpCode.RelativeInc)
                {
                    switch (opCode)
                    {
                        case OpCode.Read:
                            if (input.Count == 0)
                            {
                                return;
                            }

                            programInstructions[
                                    GetRealPosition(p1Mode, p1Instruction, programInstructions, relativeBase)] =
                                input.Pop();
                            break;
                        case OpCode.Write:
                            outputs.Add(p1);
                            break;
                        case OpCode.RelativeInc:
                            relativeBase += p1;
                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 2;
                    continue;
                }

                if (opCode == OpCode.JumpIfTrue || opCode == OpCode.JumpIfFalse)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = GetParamValue(p2Mode, p2Instruction, programInstructions, relativeBase);

                    switch (opCode)
                    {
                        case OpCode.JumpIfTrue:
                            if (p1 != 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        case OpCode.JumpIfFalse:
                            if (p1 == 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 3;
                    continue;
                }

                if (opCode == OpCode.Sum || opCode == OpCode.Mul || opCode == OpCode.LessThan ||
                    opCode == OpCode.Equals)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = GetParamValue(p2Mode, p2Instruction, programInstructions, relativeBase);

                    var p3Instruction = programInstructions[i + 3];
                    var p3RealInstruction =
                        GetRealPosition(p3Mode, p3Instruction, programInstructions, relativeBase);

                    switch (opCode)
                    {
                        case OpCode.Sum:
                            programInstructions[p3RealInstruction] = p1 + p2;
                            break;
                        case OpCode.Mul:
                            programInstructions[p3RealInstruction] = p1 * p2;
                            break;
                        case OpCode.LessThan:
                            programInstructions[p3RealInstruction] = p1 < p2 ? 1 : 0;
                            break;
                        case OpCode.Equals:
                            programInstructions[p3RealInstruction] = p1 == p2 ? 1 : 0;
                            break;
                        default:
                            throw new Exception("OpcOde");
                    }

                    i += 4;
                    continue;
                }

                throw new Exception($"OpCode {opCode} Instruction {instruction}");
            }
        }

        private static long GetParamValue(Mode mode, long programInstruction, Dictionary<long, long> program,
            long relativeBase)
        {
            if (mode == Mode.Immediate)
            {
                return programInstruction;
            }

            if (mode == Mode.Position)
            {
                if (program.TryGetValue(programInstruction, out var val))
                {
                    return val;
                }

                return 0L;
            }

            if (mode == Mode.Relative)
            {
                if (program.TryGetValue(programInstruction + relativeBase, out var val))
                {
                    return val;
                }

                return 0L;
            }

            throw new Exception("Mode!");
        }

        private static long GetRealPosition(Mode mode, long programInstruction, Dictionary<long, long> program,
            long relativeBase)
        {
            if (mode == Mode.Immediate)
            {
                throw new Exception("Cant get immideate mode with only position");
            }

            if (mode == Mode.Position)
            {
                return programInstruction;
            }

            if (mode == Mode.Relative)
            {
                return programInstruction + relativeBase;
            }

            throw new Exception("Mode!");
        }
    }
}