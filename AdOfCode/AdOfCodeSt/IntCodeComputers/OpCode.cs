﻿namespace AdOfCodeSt.IntCodeComputers
{
    public enum OpCode
    {
        Sum = 1,
        Mul = 2,
        Read = 3,
        Write = 4,
        JumpIfTrue = 5,
        JumpIfFalse = 6,
        LessThan = 7,
        Equals = 8,
        RelativeInc = 9,
        End = 99
    }
}