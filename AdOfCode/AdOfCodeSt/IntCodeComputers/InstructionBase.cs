﻿using System.Collections.Generic;

namespace AdOfCodeSt.IntCodeComputers
{
    public abstract class InstructionBase
    {
        public readonly IReadOnlyCollection<long> Params;
        public abstract InstructionResult Execute(Dictionary<long, long> programm);
    }
}