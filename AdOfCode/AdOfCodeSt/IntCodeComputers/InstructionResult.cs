﻿using System.Collections.Generic;

namespace AdOfCodeSt.IntCodeComputers
{
    public class InstructionResult
    {
        public Dictionary<long, long> NewProgram { get; }
        public long NewProgrammIterator { get; }
    }
}