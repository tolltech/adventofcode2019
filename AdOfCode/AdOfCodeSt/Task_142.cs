﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_142
    {
        [Test]
//        [TestCase(@"10 ORE => 10 A
//1 ORE => 1 B
//7 A, 1 B => 1 C
//7 A, 1 C => 1 D
//7 A, 1 D => 1 E
//7 A, 1 E => 1 FUEL", 31)]
//        [TestCase(@"10 ORE => 10 A
//1 ORE => 1 B
//7 A, 1 B => 2 C
//7 A, 1 C => 1 D
//1 C, 1 D => 1 E
//7 A, 1 E => 1 FUEL", 31)]
//        [TestCase(@"9 ORE => 2 A
//8 ORE => 3 B
//7 ORE => 5 C
//3 A, 4 B => 1 AB
//5 B, 7 C => 1 BC
//4 C, 1 A => 1 CA
//2 AB, 3 BC, 4 CA => 1 FUEL", 165)]
        [TestCase(@"157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT", 82892753 )]
        [TestCase(@"157 ORE => 5 D
165 ORE => 6 G
44 A, 5 B, 1 C, 29 D, 9 E, 48 F => 1 FUEL
12 F, 1 E, 8 H => 9 C
179 ORE => 7 H
177 ORE => 5 F
7 G, 7 H => 2 A
165 ORE => 2 E
3 G, 7 D, 5 F, 10 H => 8 B", 82892753 )]
        [TestCase(@"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF", 5586022 )]
        [TestCase(@"171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX", 460664 )]
        [TestCase(@"171 ORE => 8 Q
7 H, 3 G, 9 P, 26 J, 1 C, 2 K, 1 L => 4 D
114 ORE => 4 A
14 O => 6 G
6 A, 18 B, 12 C, 7 D, 31 E, 37 F => 1 FUEL
6 C, 2 G, 8 H, 18 B, 1 J, 6 K, 1 L => 6 E
15 M, 2 N, 1 O => 6 H
13 C, 10 N, 3 L, 14 J, 2 K, 1 H => 1 F
5 G => 4 C
189 ORE => 9 B
1 K, 17 M, 3 P => 2 J
12 O, 27 Q => 2 M
15 B, 12 A => 5 P
3 A, 2 O => 7 K
121 ORE => 7 O
7 P => 6 L
5 A, 4 O => 5 N", 460664 )]
        [TestCase(@"3 DJDNR => 1 ZCMR
7 VWJH => 5 ZPGT
5 BHZP => 2 DJDNR
6 KCNKC, 19 MZWS => 4 PKVJF
21 GXSHP, 1 TWGP, 3 BGCW => 1 XHRWR
12 DZGWQ, 2 XRDL, 3 XNVT => 2 FTMC
7 VWJH, 33 BGCW, 1 TBVC => 9 DSDP
1 NMTGB, 4 KCNKC, 5 SBSJ, 4 MCZDZ, 7 DLCP, 2 GRBZF, 1 CLKP, 10 VQHJG => 6 DVCR
7 ZCMR => 9 VNTF
2 VNTF, 1 GKMN => 1 TZWBH
6 QMFV, 7 GRBZF => 7 RHDZ
8 PKVJF => 9 NJQH
110 ORE => 9 GZTS
4 DJDNR, 7 SFHV => 8 KQFH
1 ZTCZ, 5 LZFBP => 7 VWPMZ
2 GKMN, 6 TZWBH, 1 GXSHP => 1 MJHJH
2 DLCP, 4 NGJRN => 3 GRBZF
2 DJDNR, 1 GSRBL => 4 VWJH
7 RMQX => 3 SFHV
1 GZTS => 7 GSRBL
3 GZTS, 1 SFHV => 3 QLXCS
10 SFHV => 3 MKTHL
2 DJDNR, 2 BGCW, 4 FSTJ => 3 GKMN
2 KQFH, 7 GSRBL => 7 TWGP
22 RHDZ, 22 DZGWQ, 2 NGJRN, 14 XHRWR, 21 VWPMZ, 15 ZPXHM, 26 BHZP => 8 BPHZ
1 QLXCS => 6 ZBTS
12 DLCP, 9 DSDP => 9 ZPXHM
1 VNTF => 5 ZBTX
2 TZWBH, 2 JCDW => 1 CPLG
1 XHRWR, 7 FSTJ, 5 DZGWQ => 4 NGJRN
179 ORE => 3 RMQX
1 DSDP => 1 MZWS
140 ORE => 8 BHZP
1 LZFBP, 4 DZGWQ => 2 PMDK
1 GZTS => 1 GXSHP
10 CPLG, 8 MCZDZ => 5 ZTCZ
5 ZPGT, 4 THLBN, 24 GSRBL, 40 VNTF, 9 DVCR, 2 SHLP, 11 PMDK, 19 BPHZ, 45 NJQH => 1 FUEL
9 MKTHL => 7 KCNKC
5 NGJRN => 3 QMFV
1 ZTCZ, 6 VNTF => 2 VQHJG
5 FTMC, 5 ZBTX, 1 MJHJH => 1 CLKP
7 FSTJ => 6 DLCP
1 DSDP => 5 KTML
4 LZFBP, 8 MKTHL => 7 MCZDZ
1 SFHV => 1 DZGWQ
2 QLXCS => 4 ZMXRH
3 KQFH, 1 DJDNR => 7 TBVC
5 DSDP => 7 THLBN
9 BHZP, 1 VWJH => 6 BGCW
4 GXSHP => 6 JCDW
1 KQFH, 3 ZMXRH => 9 XNVT
6 TBVC => 4 GVMH
3 VWPMZ, 3 GRBZF, 27 MJHJH, 2 QMFV, 4 NMTGB, 13 KTML => 7 SHLP
1 GVMH => 2 FSTJ
2 VQHJG, 2 NJQH => 8 SBSJ
1 XNVT => 2 XRDL
2 KCNKC => 5 LZFBP
2 ZBTS, 8 DLCP => 4 NMTGB", 4052920)]
        public void Test1(string input, int expected)
        {
            var lines = input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            var reactions = lines
                .Select(line =>
                {
                    var splits = line.Split(new[] {"=>"}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim())
                        .ToArray();
                    var reagents = splits[0].Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Trim()).ToArray();

                    return new Reaction
                    {
                        Reagents = reagents.Select(Parse).ToArray(),
                        Result = Parse(splits[1])
                    };
                })
                .ToArray();
            Solve(reactions).Should().Be(expected);
        }

        private static Reagent Parse(string str)
        {
            var splits = str.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            return new Reagent
            {
                Count = int.Parse(splits[0]),
                Name = splits[1].Trim()
            };
        }

        private static long Solve(Reaction[] reactions)
        {
            var reactionsByResult = reactions.ToDictionary(x => x.Result.Name);
            var firstLevelReagents = reactions.Where(x => x.Reagents.SingleOrDefault(y => y.Name == "ORE") != null)
                .Select(x => x.Result.Name).Distinct().ToHashSet();

            var rests = new Dictionary<string, long>();
            var startOreCount = 1000000000000;
            var oreCount = startOreCount;

            var c = 0L;
            while (oreCount > 0)
            {
                var spentOre = Count(reactionsByResult, firstLevelReagents, "FUEL", rests);
                oreCount -= spentOre;
                if (oreCount < 0)
                {
                    break;
                }

                ++c;

                if (rests.All(x => x.Value == 0))
                {
                    var totalSpentOre = startOreCount - oreCount;
                    return c * startOreCount / totalSpentOre;
                }
            }

            return c;
        }

        private static long Count(Dictionary<string, Reaction> reactionsByResult, HashSet<string> firstLevelReagents,
            string rootReagentName, Dictionary<string, long> rests)
        {
            var fuelReacttion = reactionsByResult[rootReagentName];
            var currentReactions = fuelReacttion.Reagents.Select(x => x).ToDictionary(x => x.Name, x => x.Count);
            while (true)
            {
                if (currentReactions.Keys.Count == 1 || currentReactions.Keys.All(firstLevelReagents.Contains))
                {
                    break;
                }

                foreach (var reagentName in currentReactions.Keys.ToArray())
                {
                    if (reagentName == "ORE" || firstLevelReagents.Contains(reagentName))
                    {
                        continue;
                    }

                    ReplaceReagents(reactionsByResult, reagentName, currentReactions, rests);
                }
            }

            foreach (var reagentName in currentReactions.Keys.ToArray())
            {
                if (reagentName == "ORE")
                {
                    continue;
                }

                ReplaceReagents(reactionsByResult, reagentName, currentReactions, rests);
            }

            return currentReactions.Single().Value;
        }

        private static void ReplaceReagents(Dictionary<string, Reaction> reactionsByResult, string reagentName,
            Dictionary<string, long> currentReactions, Dictionary<string, long> rests)
        {
            var resultReaction = reactionsByResult[reagentName];
            var needResultCount = currentReactions[reagentName];

            if (rests.TryGetValue(reagentName, out var restCount))
            {
                if (needResultCount >= restCount)
                {
                    needResultCount -= restCount;
                    rests[reagentName] = 0;
                }
                else
                {
                    rests[reagentName] -= needResultCount;
                    needResultCount = 0;
                }
            }

            if (needResultCount == 0)
            {
                currentReactions.Remove(reagentName);
                return;
            }

            var k = needResultCount / resultReaction.Result.Count;
            if (k * resultReaction.Result.Count < needResultCount)
            {
                k++;
            }

            var resultCount = k * resultReaction.Result.Count;
            var reagentsForReplace = new List<(string Name, long Count)>();
            foreach (var reagent in resultReaction.Reagents)
            {
                reagentsForReplace.Add((reagent.Name, reagent.Count * k));
            }

            if (!rests.ContainsKey(reagentName))
            {
                rests[reagentName] = 0;
            }

            rests[reagentName] += resultCount - needResultCount;

            currentReactions.Remove(reagentName);
            foreach (var reagent in reagentsForReplace)
            {
                if (!currentReactions.TryGetValue(reagent.Name, out _))
                {
                    currentReactions[reagent.Name] = 0;
                }

                currentReactions[reagent.Name] += reagent.Count;
            }
        }

        private static ILookup<string, long> Normalize(ILookup<string, long> currentReactions)
        {
            return currentReactions.ToLookup(x => x.Key, x => x.Sum());
        }


        private class Reaction
        {
            public Reagent[] Reagents { get; set; }
            public Reagent Result { get; set; }
        }

        private class Reagent
        {
            public string Name { get; set; }
            public long Count { get; set; }
        }
    }
}