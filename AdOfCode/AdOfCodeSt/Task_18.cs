﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_18
    {
        [Test]
        [TestCase(@"
#########
#b.A.@.a#
#########", 8)]
        [TestCase(@"
########################
#f.D.E.e.C.b.A.@.a.B.c.#
######################.#
#d.....................#
########################", 86)]
        [TestCase(@"
########################
#...............b.C.D.f#
#.######################
#.....@.a.B.c.d.A.e.F.g#
########################", 132)]
        [TestCase(@"
#################
#i.G..c...e..H.p#
########.########
#j.A..b...f..D.o#
########@########
#k.E..a...g..B.n#
########.########
#l.F..d...h..C.m#
#################", 136)]
        [TestCase(@"
########################
#@..............ac.GI.b#
###d#e#f################
###A#B#C################
###g#h#i################
########################", 81)]
        [TestCase(@"
#################################################################################
#.......#....f........#............c....#.........................#.....#.......#
#.#####.#.###.#########.#######.#######.#.#.#####################.#.###.#.#.###.#
#...#...#.#...#...........#.....#.......#.#l#.....#.....#.......#...#...#.#...#r#
###.#.#.#.#.#.#.###########.#####.#######.###.###.#.###.###.#########.#######.#.#
#...#.#.#.#.#.#.......#...#.#...#.#.....#..o..#.#.#...#...#.........#.........#.#
#.###.#.#.#.###.#######.#.#.###.#.###.#.#.#####.#.#.#####.#.#####.#.###########.#
#...#.#.#.#.....#.......#...#...#.....#.#.#.....#.H.#.....#.#...#.#.........#...#
#.###.#.#.#######.###########.#########.#.#####.#####.#####.#.###.#########.#.###
#.#...#.#...#.#...#...............#.....#...#.......#.....#.#...#.....#.....#...#
#.#.#######.#.#.###.#########.###.#.#######.#.#####.#####.#.###.#####.###.#####.#
#.#..q....#.#.#...#...#.....#...#...#...#...#...#...#...#.#.....#...#...#.#...#.#
#.#######.#.#.###.###.#.###.#######.#.###.#####.#.###.#.#.#####.#K#####.###.#.#.#
#.......#.#.....#...#.#.#.#.#.....#.#...#.......#...#.#.#.#.....#.#.N...#...#...#
###.#####.#####.###.###.#.#.#.###.#.###.#####.#######.#.#.#.#####.#.###.#.#######
#...#.....#.#.....#.#...#.....#.#.#...#.#...#.#.....#.#...#.......#...#.#.......#
#####.###.#.#.#####.#.#########.#.###.#.#.###.#.###.#.#########.#####.#.#######.#
#.....#...#.#.#.....#.....#.....#.#...#.#.#...#.#.....#...#.....#.....#.....#.#.#
#.#####.###.#.#.#.#######.#.###.#.#.###.#.#.###.#.#####.#.#.#####.#########.#.#.#
#...#.......#.#.#.#.......#.#...#.#.....#...#...#...#...#.#.....#...S.#.....#.#.#
#.#.#########.#.###.#######.#.###.#######.###.#######.###.#####.#####.#.#####.#.#
#n#.#..s......#.#...#.....#.#...#...#...#.#...#.....#.#.#.....#....v#.#.......#.#
#.#.#.#########.#.#.#####.#####.###.#.#.#.#.###.###.#.#.###########.#.#########M#
#.#...#.#.....#.#.#...#.#.#.......#...#.#.#...#.#.#.#.#.........#...#.....#.....#
#.#####.#.###.#.#.###.#.#.#.###########.#####.#.#.#.#.###E###.#.#.#######.#.#####
#...#...#...#...#...#...#.#...#.........#.....#.#.#.#...#.#.#.#.#...#...#...#...#
###.#.#.###.#######.#####.###.#.#########.###.#.#.#.###.#.#.#.#####.###.#####.#.#
#.#.#.#...#...#...#.......#..w#.........#.#...#...#...#.#...#...#...#...Z.#...#.#
#.#.#.###.###.#.#.#########.###########.#.#J###.###.###.#######.#.###.###.#.###.#
#.#.#...#...#...#.....#.......#.....#...#.#.#...#...#...#a....#.#.....#...#.#.#.#
#.#.#####.#.#######.###.#####.#.###.#.###.#.#.###.###.###.###.#.#######.###X#.#.#
#.#.....#.#.......#...#...#.#...#...#...#.#.#.#.#.....#.....#.....#...#.....#.#.#
#.#####.#.###########.###.#.#####.#.###.#.#.#.#.#######.###.#####.#.#########.#.#
#.......#.#.............#...#...#.#.#...#.#.#.#...#.......#.#...#.#.#.....#.#.D.#
#.#######.#.#####.#########.#.#.#.###.###.###.#.#.#######.#.#I#.#A#.#.#.#.#.#.###
#.#.......#.#.....#...#...#...#.#.....#.#...#...#.....#...#...#.#...#.#.#..t#.#.#
#.#######.#.#.#####.#.#.#.#####.#######.#.#.#########.###.#####.#####.#.#####.#.#
#.........#.#...#...#.#.#b......#...#...#.#.........#...#...#..i#...#.#.....#.#.#
#########.#.#####.###.#.#########.#.#.#.#.#.###########.###.#.###.#.#.#####.#.#.#
#...G.....#.........#.............#...#...#.............#e..#...T.#.......#y....#
#######################################.@.#######################################
#.#.....#.............#.....#...............#.......#.....#...#.................#
#.#.#.###.#.#########.#U###.#####.#####.#.#.#.#####.#.###.###.#.###############.#
#.#.#.....#.#.........#...#.#...#.#...#.#.#...#...#...#.#...#...#...........#...#
#.#.#######.#####.#######.#.#.#.###.#.#.#.#######.#####.###.#####.#########.#.#.#
#.#.....#.#.....#...#.....#...#.....#.#.#.#.....#...#.............#.......#u#.#.#
#.#####.#.#####.###.#.###############.#.#.#.###.#.###B#################.#.#.#.#.#
#.#...#...#...#.#.....#...#..x..#...#.#.#.#.#.#.#.......#...........#...#.#...#.#
#.#.#.###.#.###.#.#####.#.#.###.###.#.#.#.#.#.#.###.#####.#########.#.#########.#
#.#.#.......#...#.#...#.#...#.#...#...#.#.#...#...#...#...#.....#...#.........#.#
#.#.#########.#####.#.#.#####.###.#.###.#.###.###.#####.###.###.#.#####.#.#.###.#
#.....#.......#...#.#z#.#.....#...#...#.#...#...#...........#...#.....#.#.#.#...#
#.#####.#####.#.#.#V#.#.#.###.#.#####.#.#.#.#.#######.###############.#.#.###.###
#...#...#.#...#.#.#.#...#...#.#d..#.#.#.#j#...#.....#...#.......#.....#g#.Q.#...#
#####.###.#.###.#.#.#####.#.#.###.#.#.#.#.#####.###.#####.#####.#.#########.###.#
#.....#.....#.#.#...#.....#.#...#.#.#...#...#.#.#.#.#.....#...#...#.........#...#
#.#####.#####.#.###########.#####.#.###.###.#.#.#.#.#.#####.#.#########.#####.###
#.....#.....L.#.#...........#...#.#.....#...#.#.#.#.#.#...#.#...........#.....#.#
#.###.#####.###.#####.#######.#.#.#.#####.###.#.#.#.#.#.#.#.#.#########.#.#####.#
#...#...P.#.#...#.....#.Y...#.#...#...#.#.#...#.#.....#.#.#.#.........#.#.......#
#.#######.#.#.###.#.###.###.#.#######.#.#.###.#.#.#######.#.###.#####.#########.#
#.#.....#.#.#.#...#.#...#.#...#.....#...#...#.#.#.#.....#...#.#.#...#.....#.#...#
#.#.###.#.#.#.#.###.#####.#####.###.###.###.#.#.###.###.#####.###.#.###.#.#.#.###
#.#...#...#.#..k#.#.......#...#.#...#...#...#.#.......#.....#.....#.#...#...#.#.#
#.###.###########.#######.#.#.#.###.#.###.###.#########.###.#######.#.#####.#.#.#
#.#.#.........#.......#...#.#.#...#.#...#.#...#.......#.#.......#...#...#...#...#
#.#.#########.#.#####.#.###.#.###.#####.#.###.#.#####.#.#.#######.#####.###.###.#
#.#.........#.#.#...#.#.....#...#.......#.....#.#.#...#.#.#.......#...#...#.#.O.#
#.#######.#.#.#.###.#.#########.#.#########.###.#.#.#####.#.#######.#.###.###.###
#...#.....#.#.#.....#...#.......#.#.....#...#...#.#.......#.#...#...#...#.#...#.#
###.#.#####.#.#####.#.###.#######.###.###.###.###.###.#####.#.#.###.###.#.#.###.#
#.#...#.#...#.......#...#...#...#...#...#...#...#.....#.....#.#...#.#.....#.#...#
#.#####.#.#############.###.#.#.###.###.#######.#####.#.#####.###.#.#####.#.#.#.#
#...#...#.....#.#.......#...#.#.#...#...#.......#...#.#.#...#...#.#...#...#.#p#.#
#.###.#.#####.#.#.###.###.###.#.#.###.###.#######F#.###.#.#.###.#.###.#.###.#.###
#.#...#.....#.#.#.#...#...#...#.#...#...#.........#.....#.#.....#.....#...#.#...#
#.#.###.#####.#.#.#####.###.#W#####.#.#.#################.#####.###########.###.#
#m#...#.#.....#.#.#.....#.#h#.#.....#.#.#.....#.....#...#...#...#.........#.....#
#.###.#.#R#####.#.#.#####.#.#.#.#######.#.###.#.###.#.#C#.#.#####.#######.#####.#
#.....#.........#...#.......#...........#...#.....#...#...#.............#.......#
#################################################################################", 0)]
        [TestCase(@"
########################
#f.D...e.............@E#
######################d#
#......................#
########################
", 52)]
        [TestCase(@"
########################
#f.D.E.e.............@.#
######################d#
#......................#
########################
", 24)]
        public void Test1(string input, int expected)
        {
            var lines = input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            lines.Select(x => x.Length).Distinct().Count().Should().Be(1);

            var maxX = lines.First().Length;
            var maxY = lines.Length;
            var map = new Dictionary<(int X, int Y), string>();

            for (var y = 0; y < maxY; ++y)
            for (var x = 0; x < maxX; ++x)
            {
                map[(x, y)] = lines[y][x].ToString();
            }

            Solve(map).Should().Be(expected);
        }

        private int Solve(Dictionary<(int X, int Y), string> map)
        {
            var doors = map.Where(x => char.IsUpper(x.Value.Single())).ToDictionary(x => x.Value, x => x.Key);
            var keys = map.Where(x => char.IsLower(x.Value.Single())).ToDictionary(x => x.Value, x => x.Key);

            var droid = map.Single(x => x.Value == "@").Key;


            var nodes = BuildNodes(map, droid);

            var result = 0;
            var currentNode = nodes["@"];
            var sb = new StringBuilder();
            while (keys.Count > 0)
            {
                var closestKeys = currentNode.Neighbours.Where(x => x.Value.IsKey()).ToArray();

                var childKeysToCLosestKey = new Dictionary<string, (string Value, int Edge)>();
                var keysChains = new Dictionary<string, string[]>();
                var keysAndDoorsChains = new Dictionary<string, string[]>();
                foreach (var closestKey in closestKeys)
                {
                    childKeysToCLosestKey[closestKey.Value] = closestKey;
                    var keysChain = GetChildren(closestKey.Value, nodes, new HashSet<string> {currentNode.Value}, s => char.IsLower(s.Single())).ToArray();
                    keysChains[closestKey.Value] = new []{closestKey.Value}.Concat(keysChain.Select(x=>x.Value)).ToArray();

                    var keysAndDoorsChain = GetChildren(closestKey.Value, nodes, new HashSet<string> { currentNode.Value }, s => char.IsLetter(s.Single())).ToArray();
                    keysAndDoorsChains[closestKey.Value] = new[] { closestKey.Value }.Concat(keysAndDoorsChain.Select(x => x.Value)).ToArray();
                    foreach (var tuple in keysChain)
                    {
                        childKeysToCLosestKey[tuple.Value] = closestKey;
                    }
                }

                var closestKeyWithoutDoorsAfter = keysAndDoorsChains
                    .Where(x => x.Value.All(c => char.IsLower(c.Single())))
                    .OrderBy(x => childKeysToCLosestKey[x.Key].Edge)
                    .Select(x => x.Key)
                    .FirstOrDefault();

                var keyFromChainWithOtherChildDoor = childKeysToCLosestKey.Keys
                    .Where(x => doors.ContainsKey(x.ToUpper()))
                    .OrderBy(x=> childKeysToCLosestKey[x].Edge)
                    .FirstOrDefault(x =>
                    {
                        var children = GetChildren(x, nodes, new HashSet<string> {currentNode.Value},
                            s => char.IsLetter(s.Single()) || s == "@").ToArray();
                        return children.All(c => c.Value != x.ToUpper());
                    });

                var chosenKey = closestKeyWithoutDoorsAfter != null &&
                                childKeysToCLosestKey[closestKeyWithoutDoorsAfter].Edge ==
                                closestKeys.Select(x => x.Edge).Min()
                    ? closestKeyWithoutDoorsAfter
                    : keyFromChainWithOtherChildDoor;

                var keyToCollect = chosenKey != null
                    ? closestKeys.Single(x => x == childKeysToCLosestKey[chosenKey])
                    : closestKeys.OrderBy(x => x.Edge).First();

                var chosenChain = keysChains[keyToCollect.Value];

                var node = currentNode;
                foreach(var ch in chosenChain)
                {
                    result += node.Neighbours.Single(x => x.Value == ch).Edge;
                    node = nodes[ch];

                    if (doors.TryGetValue(ch.ToUpper(), out var doorPosition))
                    {
                        map[doorPosition] = ".";
                        doors.Remove(ch.ToUpper());
                    }

                    droid = keys[node.Value];
                    map[keys[ch]] = ".";
                    keys.Remove(ch);

                    if (ch == keyFromChainWithOtherChildDoor || keyFromChainWithOtherChildDoor == null)
                    {
                        break;
                    }
                }

                map[droid] = "@";

                sb.AppendLine(RenderMap(map, null));
                sb.AppendLine(keyToCollect.Edge.ToString());
                sb.AppendLine();
                nodes = BuildNodes(map, droid);

                currentNode = nodes["@"];
            }

            var ss = sb.ToString();
            return result;
        }

        private Dictionary<string, Node> BuildNodes(Dictionary<(int X, int Y), string> map, (int X, int Y) droid)
        {
            var nodes = new Dictionary<string, Node>();

            var positions = new Stack<(int X, int Y)>();
            positions.Push(droid);
            var visited = new HashSet<string>();

            while (positions.Count > 0 )
            {
                var position = positions.Pop();

                var currentNode = new Node
                {
                    Value = map[position]
                };
                nodes[currentNode.Value] = currentNode;
                visited.Add(currentNode.Value);

                var nextDoorOrKeys = GetNextDoorOrKeys(position, 0, map, new HashSet<(int X, int Y)>()).ToArray();
                foreach (var nextDoorOrKey in nextDoorOrKeys.Where(x => !visited.Contains(x.Value)))
                {
                    currentNode.Neighbours.Add(nextDoorOrKey);
                    var childPosition = map.Single(x => x.Value == nextDoorOrKey.Value);
                    visited.Add(childPosition.Value);
                    positions.Push(childPosition.Key);
                }
            }

            map[droid] = ".";

            return nodes;
        }

        private (string Value, int Edge)[] GetChildren(string node, Dictionary<string, Node> nodes, HashSet<string> excepted, Func<string, bool> predicate)
        {
            var list = new List<(string Value, int Edge)>();
            var children = nodes[node].Neighbours.Where(x => !excepted.Contains(x.Value)).ToArray();

            excepted.Add(node);

            foreach (var child in children)
            {
                if (!predicate(child.Value))
                {
                    continue;
                }

                list.Add(child);
                list.AddRange(GetChildren(child.Value, nodes, excepted, predicate));
            }

            excepted.Remove(node);

            return list.ToArray();
        }

        private (string Value, int StepsCount)[] GetNextDoorOrKeys((int X, int Y) currentLevelCell,
            int currentLevel, Dictionary<(int X, int Y), string> map, HashSet<(int X, int Y)> visited)
        {
            var neighbours = directions
                .Select(direction => Step(currentLevelCell, direction))
                .Where(x => !visited.Contains(x))
                .Where(x => map.TryGetValue(x, out var val) && val != "#")
                .Select(x => (Position: x, Value: map[x]))
                .ToArray();

            if (neighbours.Length == 0)
            {
                return Array.Empty<(string Value, int StepsCount)>();
            }

            var list = new List<(string Value, int StepsCount)>();
            list.AddRange(neighbours.Where(x => char.IsLetter(x.Value.Single()) || x.Value == "@").Select(x => (x.Value, currentLevel + 1)));

            var points = neighbours.Where(x => x.Value == ".").Select(x => x.Position).ToArray();

            visited.Add(currentLevelCell);

            list.AddRange(points.SelectMany(x => GetNextDoorOrKeys(x, currentLevel + 1, map, visited)));

            visited.Remove(currentLevelCell);

            return list.ToArray();
        }

        private string RenderMap(Dictionary<(int X, int Y), string> map, Dictionary<(int X, int Y), int> markedMap)
        {
            var maxX = map.Keys.Select(x => x.X).Max();
            var maxY = map.Keys.Select(x => x.Y).Max();
            var sb = new StringBuilder();
            for (var y = 0; y <= maxY; ++y)
            {
                for (var x = 0; x <= maxX; ++x)
                {
                    sb.Append(map[(x, y)]);
                    //if (markedMap.TryGetValue((x, y), out var marked))
                    //{
                    //    sb.Append(marked);
                    //}
                    //else
                    //{
                    //    sb.Append(map[(x, y)]);
                    //}
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        private (int X, int Y) Step((int X, int Y) droid, (int X, int Y) direction)
        {
            return (X: droid.X + direction.X, Y: droid.Y + direction.Y);
        }

        private readonly (int X, int Y)[] directions = new[]
        {
            Up,
            Right,
            Down,
            Left,
        };

        private static readonly (int X, int Y) Up = (0, -1);
        private static readonly (int X, int Y) Left = (-1, 0);
        private static readonly (int X, int Y) Down = (0, 1);
        private static readonly (int X, int Y) Right = (1, 0);

        private class Node
        {
            public Node()
            {
                Neighbours = new List<(string Value, int Edge)>();
            }

            public string Value { get; set; }
            public List<(string Value, int Edge)> Neighbours { get; set; }

            public bool IsKey => char.IsLower(Value.Single());
            public bool IsDoor => char.IsUpper(Value.Single());
        }
    }

    public static class TupleExt
    {
        public static bool IsKey(this string str)
        {
            return char.IsLower(str.Single());
        }
        
        public static bool IsDoor(this string str)
        {
            return char.IsUpper(str.Single());
        }
    }
}