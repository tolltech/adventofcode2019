﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdOfCodeSt.IntCodeComputers;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_192
    {
        [Test]
        [TestCase(new long[]
        {
            109, 424, 203, 1, 21102, 11, 1, 0, 1106, 0, 282, 21101, 18, 0, 0, 1106, 0, 259, 2101, 0, 1, 221, 203, 1,
            21102, 31, 1, 0, 1106, 0, 282, 21102, 1, 38, 0, 1105, 1, 259, 20102, 1, 23, 2, 22101, 0, 1, 3, 21101, 0, 1,
            1, 21101, 0, 57, 0, 1106, 0, 303, 1202, 1, 1, 222, 21001, 221, 0, 3, 20102, 1, 221, 2, 21102, 259, 1, 1,
            21101, 80, 0, 0, 1105, 1, 225, 21102, 1, 149, 2, 21101, 0, 91, 0, 1105, 1, 303, 1202, 1, 1, 223, 21002, 222,
            1, 4, 21102, 259, 1, 3, 21102, 225, 1, 2, 21102, 225, 1, 1, 21101, 118, 0, 0, 1105, 1, 225, 20102, 1, 222,
            3, 21101, 0, 127, 2, 21102, 133, 1, 0, 1105, 1, 303, 21202, 1, -1, 1, 22001, 223, 1, 1, 21102, 1, 148, 0,
            1106, 0, 259, 1201, 1, 0, 223, 21001, 221, 0, 4, 21002, 222, 1, 3, 21102, 14, 1, 2, 1001, 132, -2, 224,
            1002, 224, 2, 224, 1001, 224, 3, 224, 1002, 132, -1, 132, 1, 224, 132, 224, 21001, 224, 1, 1, 21101, 195, 0,
            0, 106, 0, 108, 20207, 1, 223, 2, 20102, 1, 23, 1, 21101, 0, -1, 3, 21102, 214, 1, 0, 1106, 0, 303, 22101,
            1, 1, 1, 204, 1, 99, 0, 0, 0, 0, 109, 5, 1202, -4, 1, 249, 22102, 1, -3, 1, 21201, -2, 0, 2, 21201, -1, 0,
            3, 21102, 1, 250, 0, 1105, 1, 225, 22102, 1, 1, -4, 109, -5, 2106, 0, 0, 109, 3, 22107, 0, -2, -1, 21202,
            -1, 2, -1, 21201, -1, -1, -1, 22202, -1, -2, -2, 109, -3, 2105, 1, 0, 109, 3, 21207, -2, 0, -1, 1206, -1,
            294, 104, 0, 99, 21202, -2, 1, -2, 109, -3, 2106, 0, 0, 109, 5, 22207, -3, -4, -1, 1206, -1, 346, 22201, -4,
            -3, -4, 21202, -3, -1, -1, 22201, -4, -1, 2, 21202, 2, -1, -1, 22201, -4, -1, 1, 22101, 0, -2, 3, 21101,
            343, 0, 0, 1106, 0, 303, 1106, 0, 415, 22207, -2, -3, -1, 1206, -1, 387, 22201, -3, -2, -3, 21202, -2, -1,
            -1, 22201, -3, -1, 3, 21202, 3, -1, -1, 22201, -3, -1, 2, 22101, 0, -4, 1, 21102, 1, 384, 0, 1106, 0, 303,
            1105, 1, 415, 21202, -4, -1, -4, 22201, -4, -3, -4, 22202, -3, -2, -2, 22202, -2, -4, -4, 22202, -3, -2, -3,
            21202, -4, -1, -2, 22201, -3, -2, 1, 22102, 1, 1, -4, 109, -5, 2106, 0, 0
        }, 1826L, 1982L)]
        public void Test1(long[] inputs, long expectedX, long expectedY)
        {
            programm = inputs;
            var currentX = 8L;
            var currentY = 9L;
            var startXOfYLine = new Dictionary<long, long> {{currentY, currentX}};
            var wide = 100;

            while (true)
            {
                //var s = RenderMap(currentX, currentY, wide * 3);

                //if (currentY == 60 && currentX == 55)
                //{
                //    var ssss = "";
                //}

                //s = RenderMap(currentX, currentY, wide * 3);

                if (Check(currentX, currentY) && Check(currentX, currentY + wide - 1) && Check(currentX + wide - 1, currentY))
                {
                    break;
                }

                if (Check(currentX + wide, currentY))
                {
                    ++currentX;
                    continue;
                }

                currentX = startXOfYLine[currentY];
                ++currentY;
                if (!Check(currentX, currentY))
                {
                    ++currentX;
                }

                startXOfYLine[currentY] = currentX;
            }

            var s2 = RenderMap(currentX - 100, currentY - 100, 100 + wide);

            currentX.Should().Be(expectedX);
            currentY.Should().Be(expectedY);
        }

        private bool CheckSquare(long startX, long startY)
        {
            for (var x = startX; x - startX <= 100; x += 100)
            for (var y = startY; y - startY <= 100; y += 100)
            {
                if (!Check(x, y))
                {
                    return false;
                }
            }

            return true;
        }

        private long[] programm;

        private bool Check(long x, long y)
        {
            var computer = new IntCodeComputer();
            computer.LoadProgramm(programm.ToArray());
            var outputs = computer.Compute(new[] {x, y});
            return outputs.Single() == 1L;
        }

        private static long? SafeGet(Dictionary<(long X, long Y), long> map, (long X, long Y) c)
        {
            return map.TryGetValue(c, out var r) ? r : (long?) null;
        }

        private string RenderMap(long startX, long startY, int wide = 100)
        {
            var sb = new StringBuilder();
            for (var y = startY; y - startY < wide; ++y)
            {
                for (var x = startX; x - startX < wide; ++x)
                {
                    sb.Append(Check(x, y) ? "#" : ".");
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}