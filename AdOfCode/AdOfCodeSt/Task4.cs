﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task4
    {
        [Test]
        [TestCase("111111", true)]
        [TestCase("223450", false)]
        [TestCase("203455", false)]
        [TestCase("123789", false)]
        [TestCase("122345", true)]
        [TestCase("112345", true)]
        [TestCase("123455", true)]
        public void Test1(string input, bool expected)
        {
            Solve(input).Should().Be(expected);
        }

        [Test]
        [TestCase(165432, 707912)]
        [TestCase(165433, 707911)]
        public void GetSolution(int left, int right)
        {
            var cnt = 0;
            for (var number = left; number <= right; ++number)
            {
                if (Solve(number.ToString()))
                {
                    ++cnt;
                }
            }

            cnt.Should().Be(1716);
        }

        private static bool Solve(string input)
        {
            var ints = input.Select(x => int.Parse(x.ToString())).ToArray();
            var wasRepeat = false;
            for (var i = 1; i < ints.Length; ++i)
            {
                var prev = ints[i - 1];
                var current = ints[i];

                if (prev > current)
                {
                    return false;
                }

                if (prev == current)
                {
                    wasRepeat = true;
                }
            }

            return wasRepeat;
        }
    }
}