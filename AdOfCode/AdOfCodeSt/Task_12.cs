﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_12
    {
        [Test]
        [TestCase(new[]
        {
            "-6,2,-9",
            "12,-14,-4",
            "9,5,-6",
            "-1,-4,9",
        }, 1000, 14907)]
        [TestCase(new[]
        {
            "-1,0,2",
            "2,-10,-7",
            "4,-8,8",
            "3,5,-1",
        }, 10, 179)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 10, 100 + 23 * 12 + 21 * 8 + 18 * 9)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 20, 17 * 7 + 36 * 6 + 15 * 9 + 8 * 4)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 30, 30*9+18*13+12*14+10*6)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 40, 30*14+27*10+27*3+14*13)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 60, 52*8+37*15+23*10+28*7)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 70, 44*16+24*16+21*21+21*5)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 80, 41*6+6*19+40*12+11*9)]
        [TestCase(new[]
        {
            "-8,-10,0",
            "5,5,10",
            "2,-7,3",
            "9,-8,-3",
        }, 100, 1940)]
        public void Test1(string[] input, int steps, long expected)
        {
            var moons = input.Select(x => x.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries))
                .Select(x => new XYZ(x.Select(long.Parse).ToArray()))
                .Select(x => new Moon
                {
                    Coordinates = x,
                    CurrentVelocities = new XYZ(new long[3]),
                    DeltaVelocities = new XYZ(new long[3])
                })
                .ToArray();

            Solve(moons, steps).Should().Be(expected);
        }

        private long Solve(Moon[] moons, int steps)
        {
            var pairs = new (Moon Left, Moon Right)[]
            {
                (moons[0], moons[1]),
                (moons[0], moons[2]),
                (moons[0], moons[3]),
                (moons[1], moons[2]),
                (moons[1], moons[3]),
                (moons[2], moons[3]),
            };

            var currentStep = 0L;

            while (true)
            {
                for (var axeIndex = 0; axeIndex < 3; ++axeIndex)
                {
                    SetDeltaVelocities(pairs, axeIndex);
                }

                var nextExtremums = new List<double>(6);
                for (var axeIndex = 0; axeIndex < 3; ++axeIndex)
                {
                    foreach (var pair in pairs)
                    {
                        if (pair.Left.Coordinates.Axes[axeIndex] == pair.Right.Coordinates.Axes[axeIndex])
                        {
                            nextExtremums.Add(1);
                            continue;
                        }

                        var v01 = pair.Left.CurrentVelocities.Axes[axeIndex];
                        var v02 = pair.Right.CurrentVelocities.Axes[axeIndex];
                        var dV1 = pair.Left.DeltaVelocities.Axes[axeIndex];
                        var dV2 = pair.Right.DeltaVelocities.Axes[axeIndex];
                        var x01 = pair.Left.Coordinates.Axes[axeIndex];
                        var x02 = pair.Right.Coordinates.Axes[axeIndex];

                        var a = dV1 - dV2;
                        var b = a + 2 * (v01 - v02);
                        var c = 2 * (x01 - x02);
                        var D = b * b - 4 * a * c;
                        if (D <= 0)
                        {
                            continue;
                        }

                        var x1 = (-b + Math.Sqrt(D)) / 2 / a;
                        var x2 = (-b - Math.Sqrt(D)) / 2 / a;
                        var xResult = new[] {x1, x2}.Where(x => x > 0).OrderBy(x => x).ToArray();
                        if (xResult.Length > 0)
                        {
                            nextExtremums.Add(xResult[0]);
                        }
                    }
                }

                if (nextExtremums.Count == 0)
                {
                    throw new Exception("Cant find next Extremum");
                }

                var nextStepDouble = nextExtremums.Min();
                var nextStep = nextStepDouble >= 1 ? (long) nextStepDouble : 1;
                if ((currentStep + nextStep) >= steps)
                {
                    MoveToStep(moons, steps - currentStep);
                    break;
                }
                else
                {
                    MoveToStep(moons, nextStep);
                    currentStep += nextStep;
                }
            }

            return moons.Sum(x => x.Coordinates.Axes.Sum(Math.Abs) * x.CurrentVelocities.Axes.Sum(Math.Abs));
        }

        private void MoveToStep(Moon[] moons, long stepNumber)
        {
            var deltaStep = stepNumber;
            for (var axeIndex = 0; axeIndex < 3; ++axeIndex)
            {
                foreach (var moon in moons)
                {
                    var v = moon.CurrentVelocities;
                    var d = moon.DeltaVelocities;
                    var x = moon.Coordinates.Axes[axeIndex];
                    moon.Coordinates.Axes[axeIndex] = x + v.Axes[axeIndex] * deltaStep + d.Axes[axeIndex] * (1 + deltaStep) * deltaStep / 2;
                    moon.CurrentVelocities.Axes[axeIndex] = v.Axes[axeIndex] + d.Axes[axeIndex] * deltaStep;
                }
            }
        }

        private void SetDeltaVelocities((Moon Left, Moon Right)[] pairs, int axeIndex)
        {
            foreach (var pair in pairs)
            {
                pair.Left.DeltaVelocities.Axes[axeIndex] = 0;
                pair.Right.DeltaVelocities.Axes[axeIndex] = 0;
            }

            foreach (var pair in pairs)
            {
                var moon = pair.Left;
                var other = pair.Right;

                if (moon.Coordinates.Axes[axeIndex] > other.Coordinates.Axes[axeIndex])
                {
                    moon.DeltaVelocities.Axes[axeIndex]--;
                    other.DeltaVelocities.Axes[axeIndex]++;
                }

                if (moon.Coordinates.Axes[axeIndex] < other.Coordinates.Axes[axeIndex])
                {
                    moon.DeltaVelocities.Axes[axeIndex]++;
                    other.DeltaVelocities.Axes[axeIndex]--;
                }
            }
        }

        private static long GetPosition(long startPosition, long t, long deltaVelocity, long startVelocity = 0)
        {
            return startPosition + startVelocity * t + deltaVelocity * (1 + t) * t / 2;
        }

        private class XYZ
        {
            public XYZ(long[] axes)
            {
                if (axes.Length != 3)
                {
                    throw new Exception("Baad");
                }

                Axes = axes;
            }

            public readonly long[] Axes;
            public override string ToString()
            {
                return $"{Axes[0]},{Axes[1]},{Axes[2]}";
            }
        }

        private class Moon
        {
            public XYZ Coordinates { get; set; }
            public XYZ CurrentVelocities { get; set; }
            public XYZ DeltaVelocities { get; set; }
        }
    }
}