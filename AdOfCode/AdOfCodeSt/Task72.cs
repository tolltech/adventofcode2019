﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdOfCodeSt.IntCodeComputers;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task72
    {
        [Test]
        [TestCase(new[]
        {
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26,
            27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
        }, 139629729, new[] {9, 8, 7, 6, 5})]
        [TestCase(new[]
        {
            3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
            -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
            53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10
        }, 18216, new[] {9, 7, 8, 5, 6})]
        public void Test1(int[] inputs, int maxSignal, int[] bestPhase)
        {
            var ampInput = GetOutput(inputs, bestPhase);
            ampInput.Should().Be(maxSignal);
        }

        [Test]
        [TestCase(new[]
        {
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26,
            27, 4, 27, 1001, 28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
        }, 139629729, new[] {9, 8, 7, 6, 5})]
        [TestCase(new[]
        {
            3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
            -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
            53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10
        }, 18216, new[] {9, 7, 8, 5, 6})]
        [TestCase(new[]
        {
            3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 38, 63, 72, 85, 110, 191, 272, 353, 434, 99999, 3, 9, 102, 4, 9, 9,
            101, 2, 9, 9, 102, 3, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 4, 9, 102, 2, 9, 9, 1001, 9, 5, 9, 1002, 9, 5, 9, 101,
            3, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 99, 3, 9, 1001, 9, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 101, 2,
            9, 9, 102, 2, 9, 9, 1001, 9, 2, 9, 1002, 9, 4, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101,
            2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1002, 9,
            2, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102,
            2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001,
            9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9,
            1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9,
            101, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3,
            9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9,
            1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 1002, 9, 2, 9, 4, 9, 3,
            9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9,
            102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9,
            101, 2, 9, 9, 4, 9, 99
        }, 19384820, new[] {7,6,8,9,5})]
        public void TestBest(int[] inputs, int maxSignal, int[] bestPhases)
        {
            var best = (Output: int.MinValue, Phases: new[] {0, 0, 0, 0, 0});
            for (var i1 = 5; i1 < 10; i1++)
            for (var i2 = 5; i2 < 10; i2++)
            for (var i3 = 5; i3 < 10; i3++)
            for (var i4 = 5; i4 < 10; i4++)
            for (var i5 = 5; i5 < 10; i5++)
            {
                var phases = new[] {i1, i2, i3, i4, i5};

                if (phases.Distinct().Count() != 5)
                {
                    continue;
                }

                var ampInput = GetOutput(inputs, phases);

                if (best.Output <= ampInput)
                {
                    best = (ampInput, phases);
                }
            }

            best.Output.Should().Be(maxSignal);
            string.Join(string.Empty, best.Phases).Should().Be(string.Join(string.Empty, bestPhases));
        }

        private static int GetOutput(int[] programm, int[] phases)
        {
            var ampInput = 0L;
            var phasesList = new List<int>(phases);

            var programms = new Dictionary<int, IntCodeComputer>
            {
                {0, new IntCodeComputer()},
                {1, new IntCodeComputer()},
                {2, new IntCodeComputer()},
                {3, new IntCodeComputer()},
                {4, new IntCodeComputer()},
            };

            foreach (var intCodeComputer in programms)
            {
                intCodeComputer.Value.LoadProgramm(programm);
            }


            for (var amplifier = 0;; amplifier = (amplifier + 1) % 5)
            {
                var input = new Stack<long>();
                input.Push(ampInput);

                if (phasesList.Count > 0)
                {
                    input.Push(phasesList.First());
                    phasesList.RemoveAt(0);
                }

                var listOuputs = programms[amplifier].Compute(input).ToList();

                var last = listOuputs.Last();

                listOuputs.RemoveAt(listOuputs.Count - 1);

                listOuputs.Should().AllBeEquivalentTo(0);

                ampInput = last;

                if (amplifier == 4 && programms[amplifier].End)
                {
                    return (int)ampInput;
                }
            }
        }

        private static void Solve(int[] programInstructions, Stack<int> inputs, List<int> outputs,
            out (int Index, int OpCode) lastInstruction, int startInstructionIndex = 0)
        {
            for (var i = startInstructionIndex; i < programInstructions.Length;)
            {
                var instruction = programInstructions[i];
                var opCode = instruction % 100;

                lastInstruction = (Index: i, OpCode: opCode);
                if (opCode == 99)
                {
                    return;
                }

                var p1Mode = (Mode) ((instruction / 100) % 10);
                var p2Mode = (Mode) ((instruction / 1000) % 10);
                var p3Mode = (Mode) ((instruction / 10000) % 10);

                if (p3Mode != Mode.Position)
                {
                    throw new Exception("Mode!");
                }

                var p1Instruction = programInstructions[i + 1];
                var p1 = p1Mode == Mode.Immediate ? p1Instruction : programInstructions[p1Instruction];

                if (opCode == 3 || opCode == 4)
                {
                    switch (opCode)
                    {
                        case 3:
                            if (inputs.Count == 0)
                            {
                                return;
                            }

                            programInstructions[p1Instruction] = inputs.Pop();
                            break;
                        case 4:
                            outputs.Add(p1);
                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 2;
                    continue;
                }

                if (opCode == 5 || opCode == 6)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    switch (opCode)
                    {
                        case 5:
                            if (p1 != 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        case 6:
                            if (p1 == 0)
                            {
                                i = p2;
                                continue;
                            }

                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 3;
                    continue;
                }

                if (opCode == 1 || opCode == 2 || opCode == 7 || opCode == 8)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    var p3Instruction = programInstructions[i + 3];

                    switch (opCode)
                    {
                        case 1:
                            programInstructions[p3Instruction] = p1 + p2;
                            break;
                        case 2:
                            programInstructions[p3Instruction] = p1 * p2;
                            break;
                        case 7:
                            programInstructions[p3Instruction] = p1 < p2 ? 1 : 0;
                            break;
                        case 8:
                            programInstructions[p3Instruction] = p1 == p2 ? 1 : 0;
                            break;
                        default:
                            throw new Exception("OpcOde");
                    }

                    i += 4;
                    continue;
                }

                throw new Exception();
            }

            throw new Exception("baaad");
        }

        private enum Mode
        {
            Position = 0,
            Immediate = 1
        }
    }
}