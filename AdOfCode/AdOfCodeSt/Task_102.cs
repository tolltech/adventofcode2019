﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_102
    {
        [Test]
        [TestCase(
            @"
...###.#########.####
.######.###.###.##...
####.########.#####.#
########.####.##.###.
####..#.####.#.#.##..
#.################.##
..######.##.##.#####.
#.####.#####.###.#.##
#####.#########.#####
#####.##..##..#.#####
##.######....########
.#######.#.#########.
.#.##.#.#.#.##.###.##
######...####.#.#.###
###############.#.###
#.#####.##..###.##.#.
##..##..###.#.#######
#..#..########.#.##..
#.#.######.##.##...##
.#.##.#####.#..#####.
#.#.##########..#.##.
", @"
", 11, 13, 200, 604)]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##
", @"
", 8, 3, 9, 1501, @"
.#....###24...#..
##...##.13#67..9#
##...#...5.8####.
..#.....X...###..
..#.#.....#....##
")]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##
", @"
", 8, 3, 18, 404, @"
.#....###24...#..
##...##.13#67..9#
##...#...5.810111213.
..#.....X...14##..
..#.18.....17....1615
")]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##
", @"
", 8, 3, 27, 501, @"
.26....###24...#..
2324...27#.13#67..9#
2122...25...5.810111213.
..20.....X...14##..
..19.18.....17....1615
")]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....X...###..
..#.#.....#....##
", @"
", 8, 3, 36, 1403, @"
.26....29303124...33..
2324...2728.133267..934
2122...25...5.810111213.
..20.....X...143536..
..19.18.....17....1615
")]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....#...###..
..#.#.....#....##
", @"
", 8, 3, 1, 801)]
        [TestCase(
            @"
.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....#...###..
..#.#.....#....##
", @"
", 8, 3, 6, 1101)]
        public void Test1(string map, string expected, int centerX, int centerY, int asteroidNumber, int expectedXY, string resultMap = null)
        {
            var lines = map.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            var linesCopy = lines.Select(x => x.ToCharArray().Select(y => y.ToString()).ToArray()).ToArray();
            var width = lines.Select(x => x.Length).Distinct().Single();
            var height = lines.Length;

            var asteroids = new HashSet<(int X, int Y)>();

            for (var y = 0; y < height; ++y)
            for (var x = 0; x < width; ++x)
            {
                if (lines[y][x] == '#')
                {
                    asteroids.Add((X: x, Y: y));
                }
            }

            var asteroidsLength = asteroids.Count;
            var center = (X: centerX, Y: centerY);
            var cnt = 0;
            var actualAsteroid = (X: -1, Y: -1);

            var directions = SelectDirections(center, width, height)
                //.OrderBy(x => Math.Atan2(x.Y, x.X))
                .ToArray();
            //directions = directions.Skip(directions.Length / 4).Concat(directions.Take(directions.Length / 4)).ToArray();

            while (true)
            {
                var tans = new HashSet<int>();
                foreach (var direction in directions)
                {
                    var atan2 = GetIntAtan2(direction);
                    if (tans.Contains(atan2))
                    {
                        continue;
                    }

                    tans.Add(atan2);

                    var target = center;
                    while (true)
                    {
                        target = Step(target, direction);

                        if (IsOutOfMap(target, width, height))
                        {
                            break;
                        }

                        if (asteroids.Contains(target))
                        {
                            asteroids.Remove(target);
                            ++cnt;

                            linesCopy[target.Y][target.X] = cnt.ToString();

                            if (cnt == 25)
                            {
                                var s = 0;
                            }

                            if (cnt == asteroidNumber)
                            {
                                actualAsteroid = target;
                            }

                            break;
                        }
                    }

                    if (cnt == asteroidNumber)
                    {
                        break;
                    }
                }

                if (cnt == asteroidNumber || cnt > asteroidsLength)
                {
                    break;
                }
            }

            if (!string.IsNullOrWhiteSpace(resultMap))
            {
                var actualMap = string.Join("\r\n", linesCopy.Select(line => string.Join(string.Empty, line)));
                actualMap.Trim('\r', '\n').Should().Be(resultMap.Trim('\r', '\n'));
            }

            var actual = actualAsteroid.X * 100 + actualAsteroid.Y;
            actual.Should().Be(expectedXY);
        }

        private static int GetIntAtan2((int X, int Y) direction)
        {
            return (int) (Math.Atan2(direction.Y, direction.X) * 10000);
        }

        private static bool IsOutOfMap((int X, int Y) current, int width, int height)
        {
            return current.X < 0 || current.Y < 0 || current.X >= width || current.Y >= height;
        }

        private (int X, int Y) Step((int X, int Y) current, (int X, int Y) direction)
        {
            return (current.X + direction.X, current.Y + direction.Y);
        }

        private IEnumerable<(int X, int Y)> SelectDirections((int X, int Y) asteroid, int width, int height)
        {
            yield return (0, -1);

            foreach (var c in SkipNotNormalized(RightTop(asteroid, width, height)).OrderBy(x => Math.Atan2(x.Y, x.X))
                .ThenBy(x => Math.Abs(x.X) + Math.Abs(x.Y)))
            {
                yield return c;
            }

            yield return (1, 0);

            foreach (var c in SkipNotNormalized(RightDown(asteroid, width, height)).OrderBy(x => Math.Atan2(x.Y, x.X)).ThenBy(x => Math.Abs(x.X) + Math.Abs(x.Y)))
            {
                yield return c;
            }

            yield return (0, 1);

            foreach (var c in SkipNotNormalized(LeftDown(asteroid, width, height)).OrderBy(x => Math.Atan2(x.Y, x.X)).ThenBy(x => Math.Abs(x.X) + Math.Abs(x.Y)))
            {
                yield return c;
            }

            yield return (-1, 0);

            foreach (var c in SkipNotNormalized(LeftTop(asteroid, width, height)).OrderBy(x => Math.Atan2(x.Y, x.X)).ThenBy(x => Math.Abs(x.X) + Math.Abs(x.Y)))
            {
                yield return c;
            }
        }

        private IEnumerable<(int X, int Y)> SkipNotNormalized(IEnumerable<(int X, int Y)> src)
        {
            return src;
            //foreach (var tuple in src)
            //{
            //    if (tuple.X % tuple.Y == 0 && tuple.Y != 1)
            //    {
            //        continue;
            //    }

            //    if (tuple.Y % tuple.X == 0 && tuple.X != 1)
            //    {
            //        continue;
            //    }

            //    yield return tuple;
            //}
        }

        private IEnumerable<(int X, int Y)> RightTop((int X, int Y) asteroid, int width, int height)
        {
            for (var x = 1; x < width - asteroid.X; ++x)
            for (var y = asteroid.Y; y > 0; --y)
            {
                yield return (x, -y);
            }
        }

        private IEnumerable<(int X, int Y)> RightDown((int X, int Y) asteroid, int width, int height)
        {
            for (var x = width - asteroid.X - 1; x > 0; --x)
            for (var y = 1; y < height - asteroid.Y; ++y)
            {
                yield return (x, y);
            }
        }

        private IEnumerable<(int X, int Y)> LeftDown((int X, int Y) asteroid, int width, int height)
        {
            for (var x = 1; x <= asteroid.X; ++x)
            for (var y = height - asteroid.Y - 1; y > 0; --y)
            {
                yield return (-x, y);
            }
        }

        private IEnumerable<(int X, int Y)> LeftTop((int X, int Y) asteroid, int width, int height)
        {
            for (var x = asteroid.X; x > 0; --x)
            for (var y = 1; y <= asteroid.Y; ++y)
            {
                yield return (-x, -y);
            }
        }

        private static IEnumerable<T> ParseInput<T>(string filename, Func<string, T> parseLine)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadLines(path).Select(parseLine);
        }

        [Test]
        public void Test11()
        {
            var actual = Solve(ParseInput("11.txt", int.Parse).ToArray());
            actual.Should().Be(3376997);
        }

        private static int Solve(params int[] inputs)
        {
            return inputs.Sum(x => x / 3 - 2);
        }
    }
}