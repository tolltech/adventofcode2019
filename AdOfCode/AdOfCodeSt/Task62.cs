﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task62
    {
        [Test]
        [TestCase(@"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN", 4)]
        [TestCase(@"COM)YOU
COM)SAN", 0)]
        [TestCase(@"COM)YOU
COM)1
1)SAN", 1)]
        [TestCase(@"COM)1
1)3
3)YOU
YOU)4
4)5
5)6
6)SAN", 2)]
        public void Test1(string input, int expected)
        {
            Solve(input).Should().Be(expected);
        }

        private static string ParseInput(string filename)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadAllText(path);
        }

        [Test]
        public void Test61()
        {
            var actual = Solve(ParseInput("61.txt"));
            actual.Should().Be(391);
        }

        private static int Solve(string input)
        {
            var lines = input.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            var nodes = lines.Select(x =>
            {
                var splits = x.Split(new[] {")"}, StringSplitOptions.RemoveEmptyEntries);
                return (Parent: splits[0], Child: splits[1]);
            }).ToArray();

            nodes.Any(x => x.Child == "COM").Should().BeFalse();
            nodes.Any(x => x.Parent == "COM").Should().BeTrue();
            nodes.Distinct().Count().Should().Be(nodes.Length);

            var lookUp = nodes.ToLookup(x => x.Parent, x => x.Child);
            var tree = BuildTree("COM", lookUp);
            var currentNode = tree.Find("YOU");

            var result = 0;
            while (currentNode.Value != "SAN")
            {
                currentNode = currentNode.AllChilds.Contains("SAN")
                    ? currentNode.Childs.Single(x => x.AllChilds.Contains("SAN") || x.Value == "SAN")
                    : currentNode.Parent;
                ++result;
            }

            return result-2;
        }

        private static Node<string> BuildTree(string node, ILookup<string, string> lookUp, Node<string> parent = null,
            int currentLevel = 0)
        {
            var currentNode = new Node<string>
            {
                LevelNumber = currentLevel,
                Value = node,
                Parent = parent
            };
            var childs = lookUp[node].Select(x => BuildTree(x, lookUp, currentNode, currentLevel + 1)).ToArray();
            currentNode.Childs = childs;
            currentNode.AllChilds = childs.SelectMany(x => x.AllChilds).Concat(childs.Select(x => x.Value)).ToHashSet();
            return currentNode;
        }

        private class Node<T>
        {
            public Node()
            {
                Childs = Array.Empty<Node<T>>();
                AllChilds = new HashSet<T>();
            }

            public Node<T> Parent { get; set; }
            public Node<T>[] Childs { get; set; }
            public HashSet<T> AllChilds { get; set; }
            public int LevelNumber { get; set; }
            public T Value { get; set; }

            public Node<T> Find(T val)
            {
                if (Value.Equals(val))
                {
                    return this;
                }
                else if (Childs.Length == 0)
                {
                    return null;
                }
                else
                {
                    return Childs.Select(x => x.Find(val)).FirstOrDefault(x => x != null);
                }
            }
        }
    }
}