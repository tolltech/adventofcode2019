﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task8
    {
        [Test]
        [TestCase(new[] {1, 2, 2, 1, 1, 1, 7, 8, 9, 0, 1, 2}, 3, 2, 8)]
        [TestCase(new[] {1, 2, 3, 4, 5, 0, 7, 1, 2, 1, 1, 2}, 3, 2, 6)]
        [TestCase(new[] {1, 2, 3, 4, 0, 0, 7, 1, 0, 1, 1, 2}, 3, 2, 3)]
        public void Test1(int[] input, int width, int height, int expected)
        {
            Solve(input, width, height).Should().Be(expected);
        }

        [Test]
        public void Test11()
        {
            var actual = Solve(ParseInput("81.txt").ToArray(), 25, 6);
            actual.Should().Be(2048);
        }

        private static IEnumerable<int> ParseInput(string filename)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadAllText(path).Select(x => int.Parse(x.ToString()));
        }

        private static int Solve(int[] inputs, int width, int height)
        {
            var layers = new List<int[][]>();
            var layersCount = inputs.Length / width / height;

            for (var i = 0; i < layersCount; ++i)
            {
                var layer = new int[height][];

                for (var y = 0; y < height; ++y)
                {
                    layer[y] = new int [width];
                    for (var x = 0; x < width; ++x)
                    {
                        var index = (i * width * height) + y * width + x;
                        layer[y][x] = inputs[index];
                    }
                }

                layers.Add(layer);
            }

            var minZeroCountLayer = layers.OrderBy(layer => layer.SelectMany(row => row).Count(x => x == 0)).First();
            var line = minZeroCountLayer.SelectMany(x=>x).ToArray();
            return line.Count(x => x == 1) * line.Count(x => x == 2);
        }
    }
}