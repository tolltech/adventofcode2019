﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task82
    {
        [Test]
        [TestCase(new[] {0, 2, 2, 2, 1, 1, 2, 2, 2, 2, 1, 2, 0, 0, 0, 0}, 2, 2, "0110")]
        public void Test1(int[] input, int width, int height, string expected)
        {
            Solve(input, width, height).Should().Be(expected);
        }

        [Test]
        public void Test11()
        {
            var actual = Solve(ParseInput("81.txt").ToArray(), 25, 6);
            actual.Should().Be("100101111010001011001001010010100001000110010101001111011100010101001011000100101000000100111101010010010100000010010010101001001010000001001001010010");
        }

        private static IEnumerable<int> ParseInput(string filename)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadAllText(path).Select(x => int.Parse(x.ToString()));
        }

        private static string Solve(int[] inputs, int width, int height)
        {
            var layers = new List<int[][]>();
            var layersCount = inputs.Length / width / height;

            for (var i = 0; i < layersCount; ++i)
            {
                var layer = new int[height][];

                for (var y = 0; y < height; ++y)
                {
                    layer[y] = new int [width];
                    for (var x = 0; x < width; ++x)
                    {
                        var index = (i * width * height) + y * width + x;
                        layer[y][x] = inputs[index];
                    }
                }

                layers.Add(layer);
            }

            var resultLayer = new int[height][];

            for (var i = 0; i < height; ++i)
            {
                resultLayer[i] = Enumerable.Repeat(2, width).ToArray();
            }

            for (var i = 0; i < layersCount; ++i)
            {
                var layer = layers[i];
                for (var x = 0; x < width; ++x)
                for (var y = 0; y < height; ++y)
                {
                    if (resultLayer[y][x] != 2)
                    {
                        continue;
                    }

                    resultLayer[y][x] = layer[y][x];
                }
            }

            return string.Join(string.Empty, resultLayer.SelectMany(x => x));
        }
    }
}