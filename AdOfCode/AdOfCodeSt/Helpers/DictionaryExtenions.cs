﻿using System.Collections.Generic;

namespace AdOfCodeSt.Helpers
{
    public static  class DictionaryExtenions
    {
        public static TValue SafeGet<Tkey, TValue>(this IDictionary<Tkey, TValue> dict, Tkey key, TValue defaultVal = default)
        {
            if (dict.TryGetValue(key, out var val))
            {
                return val;
            }

            return defaultVal;
        }
    }
}