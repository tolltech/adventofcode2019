﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_10
    {
        [Test]
        [TestCase(
            @".#..#
.....
#####
....#
...##", @".7..7
.....
67775
....7
...87", 3, 4, 8, true)]
        [TestCase(
            @"
##..
##..
....
....
", @"
33..
33..
....
....
", 0, 0, 3, true)]
        [TestCase(
            @"
##..
##..
..#.
....
", @"
34..
44..
..3.
....
", 1, 0, 4, true)]        
        [TestCase(
            @"
......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####
", @"
", 5, 8, 33, false)]        
        [TestCase(
            @"
#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
", @"
", 1, 2, 35, false)]        
        [TestCase(
            @"
.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
", @"
", 6, 3, 41, false)]        
        [TestCase(
            @"
.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
", @"
", 11, 13, 210, false)]        
        [TestCase(
            @"
...###.#########.####
.######.###.###.##...
####.########.#####.#
########.####.##.###.
####..#.####.#.#.##..
#.################.##
..######.##.##.#####.
#.####.#####.###.#.##
#####.#########.#####
#####.##..##..#.#####
##.######....########
.#######.#.#########.
.#.##.#.#.#.##.###.##
######...####.#.#.###
###############.#.###
#.#####.##..###.##.#.
##..##..###.#.#######
#..#..########.#.##..
#.#.######.##.##...##
.#.##.#####.#..#####.
#.#.##########..#.##.
", @"
", 11, 13, 227, false)]
        public void Test1(string map, string expected, int expectedX, int expectedY, int expectedSightCount,
            bool fullMode)
        {
            var lines = map.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
            var width = lines.Select(x => x.Length).Distinct().Single();
            var height = lines.Length;

            var asteroids = new List<(int X, int Y)>();

            for (var y = 0; y < height; ++y)
            for (var x = 0; x < width; ++x)
            {
                if (lines[y][x] == '#')
                {
                    asteroids.Add((X: x, Y: y));
                }
            }

            var minAsteroid = asteroids.First();
            var minAsteroidNoSightCount = int.MaxValue;
            var asteroidsWithNoSightCount = new List<(int X, int Y, int NoSightCount)>(asteroids.Count);

            foreach (var currentAsteroid in asteroids)
            {
                var currentNoSightCount = 0;
                var blockedAsteroids = new HashSet<(int X, int Y)>(width * height);

                foreach (var direction in SelectDirections(currentAsteroid, width, height))
                {
                    var current = currentAsteroid;
                    var wasFirst = false;

                    do
                    {
                        var newCurrent = Step(current, direction);

                        if (IsOutOfMap(newCurrent, width, height))
                        {
                            break;
                        }

                        current = newCurrent;

                        if (lines[current.Y][current.X] == '#')
                        {
                            if (!wasFirst)
                            {
                                wasFirst = true;
                            }
                            else
                            {
                                if (!blockedAsteroids.Contains(current))
                                {
                                    blockedAsteroids.Add(current);
                                    ++currentNoSightCount;
                                }
                            }
                        }

                        if (currentNoSightCount > minAsteroidNoSightCount)
                        {
                            if (!fullMode) break;
                        }
                    } while (true);

                    if (currentNoSightCount > minAsteroidNoSightCount)
                    {
                        if (!fullMode) break;
                    }
                }

                asteroidsWithNoSightCount.Add((currentAsteroid.X, currentAsteroid.Y, currentNoSightCount));

                if (currentNoSightCount < minAsteroidNoSightCount)
                {
                    minAsteroid = currentAsteroid;
                    minAsteroidNoSightCount = currentNoSightCount;
                }
            }

            if (fullMode)
            {
                var expectedLines = Enumerable.Range(0, height).Select(x => Enumerable.Repeat(".", width).ToArray())
                    .ToArray();
                foreach (var asteroid in asteroidsWithNoSightCount)
                {
                    expectedLines[asteroid.Y][asteroid.X] =
                        (asteroidsWithNoSightCount.Count - asteroid.NoSightCount - 1).ToString();
                }

                var actualMap = string.Join("\r\n", expectedLines.Select(line => string.Join(string.Empty, line)));
                actualMap.Trim('\r', '\n').Should().Be(expected.Trim('\r', '\n'));
            }

            minAsteroid.X.Should().Be(expectedX);
            minAsteroid.Y.Should().Be(expectedY);
            var actualSightCount = asteroids.Count - minAsteroidNoSightCount - 1;
            actualSightCount.Should().Be(expectedSightCount);
        }

        private static bool IsOutOfMap((int X, int Y) current, int width, int height)
        {
            return current.X < 0 || current.Y < 0 || current.X >= width || current.Y >= height;
        }

        private (int X, int Y) Step((int X, int Y) current, (int X, int Y) direction)
        {
            return (current.X + direction.X, current.Y + direction.Y);
        }

        private IEnumerable<(int X, int Y)> SelectDirections((int X, int Y) asteroid, int width, int height)
        {
            yield return (0, -1);
            yield return (1, -1);
            yield return (1, 0);
            yield return (1, 1);
            yield return (0, 1);
            yield return (-1, 1);
            yield return (-1, 0);
            yield return (-1, -1);

            for (var x = 1; x < width - asteroid.X; ++x)
            for (var y = 1; y < asteroid.Y; ++y)
            {
                yield return (x, -y);
            }

            for (var x = 1; x < width - asteroid.X; ++x)
            for (var y = 1; y < height - asteroid.Y; ++y)
            {
                yield return (x, y);
            }

            for (var x = 1; x < asteroid.X; ++x)
            for (var y = 1; y < height - asteroid.Y; ++y)
            {
                yield return (-x, y);
            }

            for (var x = 1; x < asteroid.X; ++x)
            for (var y = 1; y < asteroid.Y; ++y)
            {
                yield return (-x, -y);
            }
        }

        private static IEnumerable<T> ParseInput<T>(string filename, Func<string, T> parseLine)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadLines(path).Select(parseLine);
        }

        [Test]
        public void Test11()
        {
            var actual = Solve(ParseInput("11.txt", int.Parse).ToArray());
            actual.Should().Be(3376997);
        }

        private static int Solve(params int[] inputs)
        {
            return inputs.Sum(x => x / 3 - 2);
        }
    }
}