﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_22
    {
        [Test]
        [TestCase(10, @"deal with increment 3", "0 7 4 1 8 5 2 9 6 3")]
        [TestCase(10, @"deal with increment 7
deal into new stack
deal into new stack", "0 3 6 9 2 5 8 1 4 7")]
        [TestCase(10, @"cut 6
deal with increment 7
deal into new stack", "3 0 7 4 1 8 5 2 9 6")]
        [TestCase(10, @"deal into new stack
cut -2
deal with increment 7
cut 8
cut -4
deal with increment 7
cut 3
deal with increment 9
deal with increment 3
cut -1", "9 2 5 8 1 4 7 0 3 6")]
        [TestCase(10007, @"cut 1470
deal with increment 46
cut -6481
deal with increment 70
cut 547
deal with increment 48
cut -6479
deal with increment 69
cut -5203
deal with increment 13
deal into new stack
deal with increment 73
deal into new stack
cut -6689
deal with increment 61
cut -9853
deal with increment 48
cut -9673
deal into new stack
deal with increment 3
deal into new stack
deal with increment 64
cut 5789
deal with increment 66
deal into new stack
deal with increment 70
cut -2588
deal with increment 6
deal into new stack
deal with increment 6
cut -7829
deal with increment 49
deal into new stack
deal with increment 19
cut 9777
deal into new stack
deal with increment 27
cut 6210
deal into new stack
deal with increment 12
cut 6309
deal with increment 12
cut -9458
deal with increment 5
cut 6369
deal with increment 27
cut 2278
deal with increment 42
cut 6656
deal with increment 62
cut -1424
deal with increment 25
deal into new stack
deal with increment 12
deal into new stack
cut -7399
deal into new stack
cut -8925
deal with increment 47
deal into new stack
cut 5249
deal with increment 65
cut -213
deal into new stack
cut 6426
deal with increment 22
cut -6683
deal with increment 38
deal into new stack
deal with increment 62
cut 6855
deal with increment 75
cut 4965
deal into new stack
cut -5792
deal with increment 30
cut 9250
deal with increment 19
cut -948
deal with increment 26
cut -5123
deal with increment 68
cut -604
deal with increment 41
deal into new stack
deal with increment 45
cut 5572
deal into new stack
cut 3853
deal with increment 21
cut 1036
deal into new stack
deal with increment 6
cut 8114
deal into new stack
deal with increment 38
cut -5
deal with increment 58
cut 9539
deal with increment 19", null, 2019, 0)]
        public void Test1(int cardsCount, string commandsStr, string expected, int expectedNumber = 0, int expectedValue = 0)
        {
            var commands = commandsStr.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x =>
                {
                    if (x.Contains("deal with increment "))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.Increment,
                            Parameter = int.Parse(x.Replace("deal with increment ", string.Empty))
                        };
                    }
                    if (x.Contains("cut "))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.Cut,
                            Parameter = int.Parse(x.Replace("cut ", string.Empty))
                        };
                    }
                    if (x.Contains("deal into new stack"))
                    {
                        return new ShuffleCommand
                        {
                            ShuffleType = ShuffleType.NewStack,
                            Parameter = 0
                        };
                    }
                    throw new Exception("Baad!");
                })
                .ToArray();

            var deck = Enumerable.Range(0, cardsCount).ToArray();

            foreach (var command in commands)
            {
                deck = command.Shuffle(deck);
            }

            if (!string.IsNullOrWhiteSpace(expected))
            {
                string.Join(" ", deck).Should().Be(expected);
            }

            if (expectedNumber != 0)
            {
                for (var i = 0; i < deck.Length; ++i)
                {
                    if (deck[i] == expectedNumber)
                    {
                        i.Should().Be(expectedValue);
                    }
                }
            }
        }

        private static IEnumerable<T> ParseInput<T>(string filename, Func<string, T> parseLine)
        {
            var path = Path.Combine(TestContext.CurrentContext.TestDirectory, "Input", filename);
            return File.ReadLines(path).Select(parseLine);
        }

        private class ShuffleCommand
        {
            public ShuffleType ShuffleType { get; set; }
            public int Parameter { get; set; }

            public int[] Shuffle(int[] stack)
            {
                switch (ShuffleType)
                {
                    case ShuffleType.NewStack:
                        return stack.Reverse().ToArray();
                    case ShuffleType.Cut:
                        if (Parameter > 0)
                        {
                            var right = stack.Take(Parameter);
                            var left = stack.Skip(Parameter);
                            return left.Concat(right).ToArray();
                        }
                        else
                        {
                            var right = stack.Take(stack.Length + Parameter);
                            var left = stack.Skip(stack.Length + Parameter);
                            return left.Concat(right).ToArray();
                        }
                    case ShuffleType.Increment:
                        var result = stack.ToArray();
                        var i = 0;
                        foreach (var card in stack)
                        {
                            result[i % result.Length] = card;
                            i += Parameter;
                        }

                        return result;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private enum ShuffleType
        {
            NewStack = 1,
            Cut = 2,
            Increment = 3
        }

        private static int Solve(params int[] inputs)
        {
            return inputs.Sum(x => x / 3 - 2);
        }
    }
}