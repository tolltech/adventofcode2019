﻿using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task22
    {
        [Test]
        [TestCase(new[] {1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50}, 3500, 910)]
        [TestCase(new[] {1, 0, 0, 0, 99}, 2, 0)]
        [TestCase(new[] {2, 3, 0, 3, 99}, 2, 300)]
        [TestCase(new[] {2, 4, 4, 5, 99, 0}, 2, 440)]
        [TestCase(new[] {1, 1, 1, 4, 99, 5, 6, 0, 99}, 30, 110)]
        [TestCase(new[]
            {
                1, 12, 2, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 9, 1, 19, 1, 19, 5, 23, 1, 23, 6, 27, 2, 9, 27, 31,
                1, 5, 31, 35, 1, 35, 10, 39, 1, 39, 10, 43, 2, 43, 9, 47, 1, 6, 47, 51, 2, 51, 6, 55, 1, 5, 55, 59, 2,
                59, 10, 63, 1, 9, 63, 67, 1, 9, 67, 71, 2, 71, 6, 75, 1, 5, 75, 79, 1, 5, 79, 83, 1, 9, 83, 87, 2, 87,
                10, 91, 2, 10, 91, 95, 1, 95, 9, 99, 2, 99, 9, 103, 2, 10, 103, 107, 2, 9, 107, 111, 1, 111, 5, 115, 1,
                115, 2, 119, 1, 119, 6, 0, 99, 2, 0, 14, 0
            },
            19690720, 7749)]
        [TestCase(new[]
            {
                1, 12, 2, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 9, 1, 19, 1, 19, 5, 23, 1, 23, 6, 27, 2, 9, 27, 31,
                1, 5, 31, 35, 1, 35, 10, 39, 1, 39, 10, 43, 2, 43, 9, 47, 1, 6, 47, 51, 2, 51, 6, 55, 1, 5, 55, 59, 2,
                59, 10, 63, 1, 9, 63, 67, 1, 9, 67, 71, 2, 71, 6, 75, 1, 5, 75, 79, 1, 5, 79, 83, 1, 9, 83, 87, 2, 87,
                10, 91, 2, 10, 91, 95, 1, 95, 9, 99, 2, 99, 9, 103, 2, 10, 103, 107, 2, 9, 107, 111, 1, 111, 5, 115, 1,
                115, 2, 119, 1, 119, 6, 0, 99, 2, 0, 14, 0
            },
            3516593, 1202)]
        public void Test1(int[] input, int expected, int rightAnswer)
        {
            for (var i = 0; i <= 99; ++i)
            for (var j = 0; j <= 99; ++j)
            {
                var correctInputs = input.ToArray();
                correctInputs[1] = i;
                correctInputs[2] = j;
                var result = Solve(correctInputs);
                if (result == expected)
                {
                    (i * 100 + j).Should().Be(rightAnswer);
                    return;
                }
            }
        }

        private static int Solve(int[] inputs)
        {
            for (var i = 0; i < inputs.Length; i += 4)
            {
                var op = inputs[i];
                switch (op)
                {
                    case 1:
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] + inputs[inputs[i + 2]];
                        break;
                    case 2:
                        inputs[inputs[i + 3]] = inputs[inputs[i + 1]] * inputs[inputs[i + 2]];
                        break;
                    case 99: return inputs[0];
                }
            }

            throw new Exception("baaad");
        }
    }
}