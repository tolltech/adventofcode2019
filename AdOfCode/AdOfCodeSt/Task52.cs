﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task52
    {
        [Test]
        [TestCase(new[] {1002,4,3,4,33}, null, null)]
        [TestCase(new[] {3, 0, 4, 0, 99}, 42, 42)]
        [TestCase(new[]
            {
                3,225,1,225,6,6,1100,1,238,225,104,0,1102,9,19,225,1,136,139,224,101,-17,224,224,4,224,102,8,223,223,101,6,224,224,1,223,224,223,2,218,213,224,1001,224,-4560,224,4,224,102,8,223,223,1001,224,4,224,1,223,224,223,1102,25,63,224,101,-1575,224,224,4,224,102,8,223,223,1001,224,4,224,1,223,224,223,1102,55,31,225,1101,38,15,225,1001,13,88,224,1001,224,-97,224,4,224,102,8,223,223,101,5,224,224,1,224,223,223,1002,87,88,224,101,-3344,224,224,4,224,102,8,223,223,1001,224,7,224,1,224,223,223,1102,39,10,225,1102,7,70,225,1101,19,47,224,101,-66,224,224,4,224,1002,223,8,223,1001,224,6,224,1,224,223,223,1102,49,72,225,102,77,166,224,101,-5544,224,224,4,224,102,8,223,223,1001,224,4,224,1,223,224,223,101,32,83,224,101,-87,224,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,80,5,225,1101,47,57,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1008,677,226,224,1002,223,2,223,1005,224,329,1001,223,1,223,107,226,677,224,1002,223,2,223,1006,224,344,101,1,223,223,1007,677,677,224,1002,223,2,223,1006,224,359,1001,223,1,223,8,677,226,224,102,2,223,223,1005,224,374,101,1,223,223,108,226,677,224,102,2,223,223,1006,224,389,1001,223,1,223,1008,677,677,224,1002,223,2,223,1006,224,404,1001,223,1,223,1107,677,677,224,102,2,223,223,1005,224,419,1001,223,1,223,1008,226,226,224,102,2,223,223,1005,224,434,101,1,223,223,8,226,677,224,1002,223,2,223,1006,224,449,101,1,223,223,1007,677,226,224,102,2,223,223,1005,224,464,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,479,1001,223,1,223,1107,226,677,224,1002,223,2,223,1005,224,494,1001,223,1,223,7,677,677,224,102,2,223,223,1006,224,509,101,1,223,223,1007,226,226,224,1002,223,2,223,1005,224,524,101,1,223,223,7,677,226,224,102,2,223,223,1005,224,539,101,1,223,223,8,226,226,224,1002,223,2,223,1006,224,554,101,1,223,223,7,226,677,224,102,2,223,223,1005,224,569,101,1,223,223,1108,677,226,224,1002,223,2,223,1005,224,584,101,1,223,223,108,677,677,224,1002,223,2,223,1006,224,599,101,1,223,223,107,226,226,224,1002,223,2,223,1006,224,614,101,1,223,223,1108,226,226,224,1002,223,2,223,1005,224,629,1001,223,1,223,1107,677,226,224,1002,223,2,223,1005,224,644,101,1,223,223,108,226,226,224,1002,223,2,223,1005,224,659,101,1,223,223,1108,226,677,224,1002,223,2,223,1005,224,674,1001,223,1,223,4,223,99,226
            },
            5,
            3892695)]
        [TestCase(new[] {3,9,8,9,10,9,4,9,99,-1,8}, 8, 1)]
        [TestCase(new[] {3,9,8,9,10,9,4,9,99,-1,8}, 7, 0)]
        [TestCase(new[] {3,9,8,9,10,9,4,9,99,-1,8}, 6, 0)]//positionMode eq 8
        [TestCase(new[] {3,9,7,9,10,9,4,9,99,-1,8}, 8, 0)]
        [TestCase(new[] {3,9,7,9,10,9,4,9,99,-1,8}, 7, 1)]
        [TestCase(new[] {3,9,7,9,10,9,4,9,99,-1,8}, 6, 1)]
        [TestCase(new[] {3,9,7,9,10,9,4,9,99,-1,8}, 9, 0)]//positionMode less 8
        [TestCase(new[] {3,3,1108,-1,8,3,4,3,99}, 8, 1)]
        [TestCase(new[] {3,3,1108,-1,8,3,4,3,99}, 6, 0)]
        [TestCase(new[] {3,3,1108,-1,8,3,4,3,99}, 7, 0)]//imm eq 8
        [TestCase(new[] {3,3,1107,-1,8,3,4,3,99}, 7, 1)]
        [TestCase(new[] {3,3,1107,-1,8,3,4,3,99}, 6, 1)]
        [TestCase(new[] {3,3,1107,-1,8,3,4,3,99}, 8, 0)]
        [TestCase(new[] {3,3,1107,-1,8,3,4,3,99}, 9, 0)]//imm less 8
        [TestCase(new[] {3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9}, 0, 0)]
        [TestCase(new[] {3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9}, 1, 1)]
        [TestCase(new[] {3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9}, 10, 1)]//jmp position
        [TestCase(new[] {3,3,1105,-1,9,1101,0,0,12,4,12,99,1}, 0, 0)]
        [TestCase(new[] {3,3,1105,-1,9,1101,0,0,12,4,12,99,1}, 1, 1)]
        [TestCase(new[] {3,3,1105,-1,9,1101,0,0,12,4,12,99,1}, 10, 1)]//jmp imm
        [TestCase(new[] {3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99}, 7, 999)]
        [TestCase(new[] {3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99}, 8, 1000)]
        [TestCase(new[] {3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99}, 9, 1001)]//big
        public void Test1(int[] inputs, int? input, int? expected)
        {
            var listOuputs = new List<int>();
            Solve(inputs, input, listOuputs);
            if (listOuputs.Count > 0)
            {
                var last = listOuputs.Last();

                listOuputs.RemoveAt(listOuputs.Count -1);

                listOuputs.Should().AllBeEquivalentTo(0);

                last.Should().Be(expected);
            }
        }

        private static void Solve(int[] programInstructions, int? input, List<int> outputs)
        {
            var buffer = input;

            for (var i = 0; i < programInstructions.Length; )
            {
                var instruction = programInstructions[i];
                var opCode = instruction % 100;

                if (opCode == 99)
                {
                    return;
                }

                var p1Mode = (Mode)((instruction / 100) % 10);
                var p2Mode = (Mode)((instruction / 1000) % 100);
                var p3Mode = (Mode)((instruction / 10000) % 1000);

                if (p3Mode != Mode.Position)
                {
                    throw new Exception("Mode!");
                }

                var p1Instruction = programInstructions[i + 1];
                var p1 = p1Mode == Mode.Immediate ? p1Instruction : programInstructions[p1Instruction];

                if (opCode == 3 || opCode == 4)
                {
                    switch (opCode)
                    {
                        case 3:
                            programInstructions[p1Instruction] = buffer.Value; 
                            break;
                        case 4:
                            outputs.Add(p1);
                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 2;
                    continue;
                }

                if (opCode == 5 || opCode == 6)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    switch (opCode)
                    {
                        case 5:
                            if (p1 != 0)
                            {
                                i = p2;
                                continue;
                            }
                            break;
                        case 6:
                            if (p1 == 0)
                            {
                                i = p2;
                                continue;
                            }
                            break;
                        default: throw new Exception("OpCode");
                    }

                    i += 3;
                    continue;
                }

                if (opCode == 1 || opCode == 2 || opCode == 7 || opCode == 8)
                {
                    var p2Instruction = programInstructions[i + 2];
                    var p2 = p2Mode == Mode.Immediate ? p2Instruction : programInstructions[p2Instruction];

                    var p3Instruction = programInstructions[i + 3];

                    switch (opCode)
                    {
                        case 1:
                            programInstructions[p3Instruction] = p1 + p2;
                            break;
                        case 2:
                            programInstructions[p3Instruction] = p1 * p2;
                            break;
                        case 7:
                            programInstructions[p3Instruction] = p1 < p2 ? 1 : 0;
                            break;
                        case 8:
                            programInstructions[p3Instruction] = p1 == p2 ? 1 : 0;
                            break;
                        default:
                            throw new Exception("OpcOde");
                    }

                    i += 4;
                    continue;
                }

                throw new Exception();
            }

            throw new Exception("baaad");
        }

        private enum Mode
        {
            Position = 0,
            Immediate = 1
        }
    }
}