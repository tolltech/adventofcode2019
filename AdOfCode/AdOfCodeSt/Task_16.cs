﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace AdOfCodeSt
{
    [TestFixture]
    public class Task_16
    {
        [Test]
        [TestCase(
            @"59791911701697178620772166487621926539855976237879300869872931303532122404711706813176657053802481833015214226705058704017099411284046473395211022546662450403964137283487707691563442026697656820695854453826690487611172860358286255850668069507687936410599520475680695180527327076479119764897119494161366645257480353063266653306023935874821274026377407051958316291995144593624792755553923648392169597897222058613725620920233283869036501950753970029182181770358827133737490530431859833065926816798051237510954742209939957376506364926219879150524606056996572743773912030397695613203835011524677640044237824961662635530619875905369208905866913334027160178",
            45834272)]
        [TestCase(
            @"80871224585914546619083218645595",
            24176176)]
        [TestCase(
            @"19617804207202209144916044189917",
            73745418)]
        [TestCase(
            @"69317163492948606335995924319873",
            52432133)]
        //[TestCase(
        //    @"69317163492948606335995924319873",
        //    52432133, 100, 3)]
        [TestCase(
            @"12345678",
            48226158, 1)]
        [TestCase(
            @"12345678",
            34040438, 2)]
        [TestCase(
            @"12345678",
            03415518, 3)]
        [TestCase(
            @"12345678",
            01029498, 4)]
        [TestCase(
            @"12345678",
            02949801, 4, 2)]
        [TestCase(
            @"1234123412341234",
            91526734)]
        [TestCase(
            @"1234123412341234",
            80904580, 1)]
        [TestCase(
            @"1234123412341234",
            62841234, 100, 8)]
        [TestCase(
            @"1234123412341234",
            26734628, 100, 3)]
        public void Test1(string input, int expected, int phaseNumber = 100, int offset = 0, int repeatCount = 1)
        {
            var inputs = input.Select(x => int.Parse(x.ToString())).ToArray();
            Solve(inputs, phaseNumber, offset, repeatCount).Should().Be(expected);
        }

        private int Solve(int[] inputs, int phaseNumber, int offset, int repeatCount)
        {
            var phases = new List<int[]>(phaseNumber);
            for (var i = 0; i < phaseNumber; ++i)
            {
                var prevPhase = phases.LastOrDefault() ?? inputs;
                phases.Add(GetNextPhaseRepeated(prevPhase, repeatCount));
            }

            var lastPhase = phases.Last();
            var ints = new List<int>(8);
            for (var i = 0; i < 8; ++i)
            {
                ints.Add(lastPhase[(i + offset) % lastPhase.Length]);
            }

            return int.Parse(string.Join(string.Empty, ints));
        }

        private int[] GetNextPhaseRepeated(int[] inputs, int repeatCount)
        {
            var totalCount = repeatCount * inputs.Length;
            var period = inputs.Length * 4;
            var phaseCapacity = period > totalCount ? totalCount : period;
            var phase = new int[phaseCapacity];

            for (var y = 0; y < phaseCapacity; ++y)
            {
                var acc = 0;
                for (var i = 0; i < inputs.Length; ++i)
                {
                    var deltaI = y / inputs.Length * inputs.Length;
                    var multiplier = GetMultiplier(y, deltaI + i + 1);
                    acc += inputs[i % inputs.Length] * multiplier;
                }
                phase[y] =  Math.Abs(acc) % 10;
            }

            return phase;
        }

        private static readonly int[] basePattern = new[] {0, 1, 0, -1};

        private static int GetMultiplier(int y, int x)
        {
            var repeater = y + 1;
            return basePattern[(x / repeater) % basePattern.Length];
        }
    }
}